% Load  MLD

% Rupelian no ice, glaciated (FAIS) and Chattian ice (EAIS)
mld_rupn  = rot90(ncread('../EO_data/tdluto.pfclann.nc', 'mixLyrDpth_mm_uo'));
mld_rupii = rot90(ncread('../EO_data/tdlupo.pfclann.nc', 'mixLyrDpth_mm_uo'));
mld_rupi  = rot90(ncread('../EO_data/tdwqeo.pfclann.nc', 'mixLyrDpth_mm_uo'));

% Chattian no ice, glaciated (EAIS) and Rupelian ice (FAIS)
mld_chan  = rot90(ncread('../EO_data/tdluuo.pfclann.nc', 'mixLyrDpth_mm_uo'));
mld_chai  = rot90(ncread('../EO_data/tdluqo.pfclann.nc', 'mixLyrDpth_mm_uo'));
mld_chaii = rot90(ncread('../EO_data/tdwqfo.pfclann.nc', 'mixLyrDpth_mm_uo'));

% Priabonian no ice, EAIS and FAIS
mld_prin  = rot90(ncread('../EO_data/tdwqjo.pfclann.nc', 'mixLyrDpth_mm_uo'));
mld_prii  = rot90(ncread('../EO_data/tdwqmo.pfclann.nc', 'mixLyrDpth_mm_uo'));
mld_priii = rot90(ncread('../EO_data/tdwqlo.pfclann.nc', 'mixLyrDpth_mm_uo'));

%% Seasonal (not sure if this is ever used)
% mld_rupns = rot90(ncread('../EO_data/tdluto.pfcldjf.nc', 'mixLyrDpth_mm_uo'));
% mld_chans = rot90(ncread('../EO_data/tdluuo.pfcldjf.nc', 'mixLyrDpth_mm_uo'));
% mld_rupis = rot90(ncread('../EO_data/tdlupo.pfcldjf.nc', 'mixLyrDpth_mm_uo'));
% mld_chais = rot90(ncread('../EO_data/tdluqo.pfcldjf.nc', 'mixLyrDpth_mm_uo'));
% mld_rupnw = rot90(ncread('../EO_data/tdluto.pfcljja.nc', 'mixLyrDpth_mm_uo'));
% mld_chanw = rot90(ncread('../EO_data/tdluuo.pfcljja.nc', 'mixLyrDpth_mm_uo'));
% mld_rupiw = rot90(ncread('../EO_data/tdlupo.pfcljja.nc', 'mixLyrDpth_mm_uo'));
% mld_chaiw = rot90(ncread('../EO_data/tdluqo.pfcljja.nc', 'mixLyrDpth_mm_uo'));






