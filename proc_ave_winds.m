
function [ave_windu,ave_windv,windX,windY] = proc_ave_winds(inputu, inputv, agg)
% function [ave_windu,ave_windv,windX,windY] = proc_ave_winds(inputu, inputv, agg)
% 
% Inputs:
% inputu = u component at model resolution
% inputv = v component at model resolution
% agg    = degree of aggregation, e.g. averaging every 3 or 5 vectors
% 
% Outputs:
% ave_windu = average u winds array
% ave_windv = average v winds array
% windX     = longitude array
% windY     = latiude array
% 


% this part of the function was added later, so just in case any old code
% hasn't been updated:

if ~exist('agg','var')
    agg = 3;
end
    
load_latlon

% find new dimensions for x and y once aggregated
xdim = floor(length(inputu(:,1))/agg);
ydim = floor(length(inputu(1,:))/agg);


% create an empty wind field with these new dimensions
ave_windu = zeros(xdim,ydim);
ave_windv = zeros(xdim,ydim);


% now do the averaging at the spacing of the aggregation
for i = 1:xdim;
    for j = 1:ydim;       
        ave_windu(i,j) = mean(mean(inputu(i*agg-(agg-1):i*agg,j*agg-(agg-1):j*agg)));
        ave_windv(i,j) = mean(mean(inputv(i*agg-(agg-1):i*agg,j*agg-(agg-1):j*agg)));
    end;
end;

% this will generate the spacing of where to place vectors:
% note, agg = 3 and 5 are included seperately because I'm more likey to use
% them and the placement can 
if agg == 3
    
    windlat = lat(2:3:71);
    windlon = lon(2:3:95);

else if agg == 5
        
        windlat = lat(4:5:69);
        windlon = lon(3:5:93);
        
    else
        % this should allow any agg values to be taken (not tested)
        windlat = lat((1+floor(agg/2)):agg:floor(length(lat)/agg)*agg);
        windlon = lon((1+floor(agg/2)):agg:floor(length(lat)/agg)*agg);
        
    end
end

% finally, mesh the wind lat and lon together for plotting purposes:
[windX, windY] = meshgrid(windlon, windlat);





% for i = 1:xdim;
%     for j = 1:ydim;
%         
%         ave_windu(i,j) = nanmean(nanmean(inputu(i*3-2:i*3,j*3-2:j*3)));
%         ave_windv(i,j) = nanmean(nanmean(inputv(i*3-2:i*3,j*3-2:j*3)));
% 
%     end;
% end;
