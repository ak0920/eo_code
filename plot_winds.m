%% Plot the stream function and merid heat flux in glorious HD


%% Load data

    load_sst_p_vars
    load_w_p_vars
    load greys.mat
   



%% Land sea masks

lsm_rup_proc = lsm_proc(flipud(isnan(sst_rupn)*1)); % lsm_proc(lsm_rup);
lsm_cha_proc = lsm_proc(flipud(isnan(sst_chan)*1)); % lsm_proc(lsm_cha);
lsm_pri_proc = lsm_proc(flipud(isnan(sst_prin)*1)); % lsm_proc(lsm_cha);
% close(gcf)
lsmc = (isnan(sst_chan))*1;
lsmp = (isnan(sst_prin))*1;
lsmr = (isnan(sst_rupn))*1;
grays = [1 1 1;0.85 0.85 0.85;0.2 0.7 1];

windX1 = (windX)*2;
windY1 = (windY-91)*-2;
v_sc = 2; % vector scale

lsmorx = lsm_rup_proc(1,:)/3.75+0.5;
lsmory= lsm_rup_proc(2,:)/-2.5+37;
lsmocx = lsm_cha_proc(1,:)/3.75+0.5;
lsmocy = lsm_cha_proc(2,:)/-2.5+37;
lsmopx = lsm_pri_proc(1,:)/3.75+0.5;
lsmopy = lsm_pri_proc(2,:)/-2.5+37;


%% Some other variables/set up things
v_sc=0.4;
cax = [-1e-4 1e-4];
lv = 205; % Land value, must be outside range of cax to be grey
cl = [-200:20:200]; % contour levels
lw = 2; % line width for NHF

%% Select manual placing of contours or no contours (for plotting quickly)
% con = 'manual';
con = [];

close(gcf)
figure

%% Rupelian no ice
subplot('position', [0.075 0.255 0.85 0.24])

imagesc(isnan(sst_rupn(37:73,:)))
colormap(greys);
hold on
% lrn = imagesc(~isnan(sst_rupn(37:73,:))-lv);
% set(lrn, 'AlphaData', isnan(sst_rupn(37:73,:)))
q1 = quiver(windX(1:12,:)/3.75, (windY(1:12,:)/(-2.5)),windu_rupn(1:12,:), (windv_rupn(1:12,:))*-1, 0,'k');
qU = get(q1,'UData');
qV = get(q1,'VData');
set(q1,'UData',v_sc*qU, 'VData',v_sc*qV);

% [c1,h1] = contour(str_rupn(1:70,:),cl,'k');
plot(lsmorx,lsmory-36,'k')
% clabel(c1, h1, con)
box on
axis off
text(-2.3,5, 'c','fontsize',fontsz,'fontname',font)
% Add gridlines
plot([1 96], [61 61], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [37 37], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [13 13], ':', 'color', [0.3 0.3 0.3])
plot([17 17], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([33 33], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([49 49], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([65 65], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([81 81], [1 73], ':', 'color', [0.3 0.3 0.3])






%% Rupelian ice
subplot('position', [0.075 0.005 0.85 0.24])

imagesc(isnan(sst_rupi(37:73,:)))
colormap(greys);
hold on
% lrn = imagesc(~isnan(sst_rupn(37:73,:))-lv);
% set(lrn, 'AlphaData', isnan(sst_rupn(37:73,:)))
q1 = quiver(windX(1:12,:)/3.75, (windY(1:12,:)/(-2.5)),windu_rupii(1:12,:), (windv_rupii(1:12,:))*-1, 0,'k');
qU = get(q1,'UData');
qV = get(q1,'VData');
set(q1,'UData',v_sc*qU, 'VData',v_sc*qV);

% [c1,h1] = contour(str_rupn(1:70,:),cl,'k');
plot(lsmorx,lsmory-36,'k')
% clabel(c1, h1, con)
box on
axis off
text(-2.3,5, 'd','fontsize',fontsz,'fontname',font)
% Add gridlines
plot([1 96], [61 61], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [37 37], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [13 13], ':', 'color', [0.3 0.3 0.3])
plot([17 17], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([33 33], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([49 49], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([65 65], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([81 81], [1 73], ':', 'color', [0.3 0.3 0.3])


%% Set some frame properties
subplot('position', [2 1 1 1])
imagesc(sst_rupn)
truesize([300 510])
set(gcf, 'color', 'w');



%% Priabonian no ice
subplot('position', [0.075 0.755 0.85 0.24])

imagesc(isnan(sst_prin(37:73,:)))
colormap(greys);
hold on
% lrn = imagesc(~isnan(sst_rupn(37:73,:))-lv);
% set(lrn, 'AlphaData', isnan(sst_rupn(37:73,:)))
q1 = quiver(windX(1:12,:)/3.75, (windY(1:12,:)/(-2.5)),windu_prin(1:12,:), (windv_prin(1:12,:))*-1, 0,'k');
qU = get(q1,'UData');
qV = get(q1,'VData');
set(q1,'UData',v_sc*qU, 'VData',v_sc*qV);

% [c1,h1] = contour(str_rupn(1:70,:),cl,'k');
plot(lsmopx,lsmopy-36,'k')
% clabel(c1, h1, con)
box on
axis off
text(-2.3,5, 'a','fontsize',fontsz,'fontname',font)
% Add gridlines
plot([1 96], [61 61], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [37 37], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [13 13], ':', 'color', [0.3 0.3 0.3])
plot([17 17], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([33 33], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([49 49], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([65 65], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([81 81], [1 73], ':', 'color', [0.3 0.3 0.3])

% scale bar
fill([2 18 18 2],[3 3 9 9],'w');
t1 = text(9,5.5, '4 m s^-^1','fontsize',fontsz,'fontname',font);
plot([4 7], [6 6], 'k-', 'linewidth',2);



%% Priabonian ice
subplot('position', [0.075 0.505 0.85 0.24])

imagesc(isnan(sst_prii(37:73,:)))
colormap(greys);
hold on
% lrn = imagesc(~isnan(sst_rupn(37:73,:))-lv);
% set(lrn, 'AlphaData', isnan(sst_rupn(37:73,:)))
q1 = quiver(windX(1:12,:)/3.75, (windY(1:12,:)/(-2.5)),windu_prii(1:12,:), (windv_prii(1:12,:))*-1, 0,'k');
qU = get(q1,'UData');
qV = get(q1,'VData');
set(q1,'UData',v_sc*qU, 'VData',v_sc*qV);

% [c1,h1] = contour(str_rupn(1:70,:),cl,'k');
plot(lsmopx,lsmopy-36,'k')
% clabel(c1, h1, con)
box on
axis off
text(-2.3,5, 'b','fontsize',fontsz,'fontname',font)
% Add gridlines
plot([1 96], [61 61], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [37 37], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [13 13], ':', 'color', [0.3 0.3 0.3])
plot([17 17], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([33 33], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([49 49], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([65 65], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([81 81], [1 73], ':', 'color', [0.3 0.3 0.3])


% %% Chattian no ice
% subplot('position', [0.05 0.255 0.6 0.24])
% 
% imagesc(isnan(w_chan))
% caxis(cax)
% colormap(scale_col3);
% hold on
% q1 = quiver(X, (Y-74)*(-1),u_chan, (v_chan)*-1, 0,'k');
% qU = get(q1,'UData');
% qV = get(q1,'VData');
% set(q1,'UData',v_sc*qU, 'VData',v_sc*qV);
% lrn = imagesc(~isnan(sst_chan)-lv);
% set(lrn, 'AlphaData', isnan(sst_chan))
% % [c1,h1] = contour(str_rupn(1:70,:),cl,'k');
% plot(lsmocx,lsmocy,'k')
% % clabel(c1, h1, con)
% box on
% axis off
% text(-2.3,5, 'RN','fontsize',fontsz,'fontname',font)
% % Add gridlines
% plot([1 96], [61 61], ':', 'color', [0.3 0.3 0.3])
% plot([1 96], [37 37], ':', 'color', [0.3 0.3 0.3])
% plot([1 96], [13 13], ':', 'color', [0.3 0.3 0.3])
% plot([17 17], [1 73], ':', 'color', [0.3 0.3 0.3])
% plot([33 33], [1 73], ':', 'color', [0.3 0.3 0.3])
% plot([49 49], [1 73], ':', 'color', [0.3 0.3 0.3])
% plot([65 65], [1 73], ':', 'color', [0.3 0.3 0.3])
% plot([81 81], [1 73], ':', 'color', [0.3 0.3 0.3])
% 
% 
% 
% %% Chattian ice
% subplot('position', [0.05 0.005 0.6 0.24])
% 
% imagesc(w_chai)
% caxis(cax)
% colormap(scale_col3);
% hold on
% q1 = quiver(X, (Y-74)*(-1),u_chai, (v_chai)*-1, 0,'k');
% qU = get(q1,'UData');
% qV = get(q1,'VData');
% set(q1,'UData',v_sc*qU, 'VData',v_sc*qV);
% lrn = imagesc(~isnan(sst_chan)-lv);
% set(lrn, 'AlphaData', isnan(sst_chan))
% % [c1,h1] = contour(str_rupn(1:70,:),cl,'k');
% plot(lsmocx,lsmocy,'k')
% % clabel(c1, h1, con)
% box on
% axis off
% text(-2.3,5, 'RN','fontsize',fontsz,'fontname',font)
% % Add gridlines
% plot([1 96], [61 61], ':', 'color', [0.3 0.3 0.3])
% plot([1 96], [37 37], ':', 'color', [0.3 0.3 0.3])
% plot([1 96], [13 13], ':', 'color', [0.3 0.3 0.3])
% plot([17 17], [1 73], ':', 'color', [0.3 0.3 0.3])
% plot([33 33], [1 73], ':', 'color', [0.3 0.3 0.3])
% plot([49 49], [1 73], ':', 'color', [0.3 0.3 0.3])
% plot([65 65], [1 73], ':', 'color', [0.3 0.3 0.3])
% plot([81 81], [1 73], ':', 'color', [0.3 0.3 0.3])
% 
% % q2= quiver([34 62 70 36],[62 64 36 42],[120 100 19 1.9],[0 0 0 0],0, 'linewidth',2, 'maxheadsize',0.3);
% % qU = get(q2,'UData');
% % qV = get(q2,'VData');
% % set(q2,'UData',v_sc*qU, 'VData',v_sc*qV);
% 
% % subplot('position', [0.7 0.005 0.25 0.24])
% % plot(hf_chai(:, 2),hf_rupn(:,1), '-b','linewidth',lw)
% % hold on
% % plot(hf_chai(:, 4),hf_rupn(:,1), '-r','linewidth',lw)
% % plot(hf_chai(:, 3),hf_rupn(:,1), '-k','linewidth',lw)
% % ylim([-90 90])
% % xlim([-6 6])
%  
% 
% 
% % q1= quiver([34 62 70 36],[59 61 34 40],[100 89 22 11],[0 0 0 0],0, 'linewidth',2, 'maxheadsize',0.3);
% % q2= quiver([34 62 70 36],[62 64 36 42],[120 100 19 1.9],[0 0 0 0],0, 'linewidth',2, 'maxheadsize',0.3);
% 
% % qU = get(q1,'UData');
% % qV = get(q1,'VData');
% % set(q1,'UData',v_sc*qU, 'VData',v_sc*qV);
% % qU = get(q2,'UData');
% % qV = get(q2,'VData');
% % set(q2,'UData',v_sc*qU, 'VData',v_sc*qV);
% 
% % quiver([34 65 70 36 ],[63 64 36 42 ],[120 100 19 1.9 ],[0 0 0 0 ],'linewidth',2, 'maxheadsize',0.3)
% % scale bar
% % lr = 4;
% % fill([40 58 58 40]+lr,[22 22 30 30],'w');
% % plot([41 45]+lr, [26 26], 'k-', 'linewidth',2);
% % text(47+lr,26, '20 Sv','fontsize',fontsz,'fontname',font)
% 
% % set(gca, 'xticklabel', '', 'yticklabel','', 'xtick', [], 'ytick', [])

%% Set some frame properties
subplot('position', [2 1 1 1])
imagesc(sst_rupn)
truesize([580 510])

set(gcf, 'color', 'w');




