%% Plot the SAT and SST
% Kennedy et al. 2015, Figure 3

% Load data
load_sst_sat
load col_scale3.mat % aka scale_col3 (with grey at minimum value)
load_lsm

% Setting fig properties
latlim = [-90 90]; 
lonlim = [0 360];
font = 'Times New Roman';
fontsz = 10;

% Set a soft/hard colour limit (hard includes the whole data range,
% limiting colour bar to soft removes greys for land)
collim1soft = [-30 30];
collim1hard = collim1soft*1.05;
collim2soft = [-6 6];
collim2hard = collim2soft*1.05;

% Set lower limit of colour range (so to allow greyed out land)
t_prid = t_priii-t_prin;
t_int_prid = t_int_priii-t_int_prin;
t_prid(t_prid<min(collim1soft)) = min(collim1soft);
t_int_prid(t_int_prid<min(collim1soft)) = min(collim1soft);

t_rupd = t_rupi-t_rupn;
t_int_rupd = t_int_rupi-t_int_rupn;
t_rupd(t_rupd<min(collim1soft)) = min(collim1soft);
t_int_rupd(t_int_rupd<min(collim1soft)) = min(collim1soft);

sst_prid = sst_priii-sst_prin;
sst_int_prid = sst_int_priii-sst_int_prin;
sst_prid(sst_prid<min(collim2soft)) = min(collim2soft);
sst_int_prid(sst_int_prid<min(collim2soft)) = min(collim2soft);

sst_rupd = sst_rupi-sst_rupn;
sst_int_rupd = sst_int_rupi-sst_int_rupn;
sst_rupd(sst_rupd<min(collim2soft)) = min(collim2soft);
sst_int_rupd(sst_int_rupd<min(collim2soft)) = min(collim2soft);


%% SAT Plots

maps = figure;

% Priabonian
subplot('position', [0.1 0.755 0.7 0.24])

axesm('MapProjection','Robinson', 'MapLatLim', latlim,... % eqdcylin    Robinson
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,t_prid) % Coarse grid up to LSM
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])

caxis(collim1hard)
colormap(scale_col3);

% Add smooth grid
hold on
pcolorm(lat_lsm,lon_lsm,flipud(t_int_prid));
plotm(lsm_pri_proc(2,:), lsm_pri_proc(1,:), 'k-'); % LSM

% tidy up
tightmap
box off
axis off


% Rupelian
subplot('position', [0.1 0.505 0.7 0.24])

axesm('MapProjection','Robinson', 'MapLatLim', latlim,... % eqdcylin    Robinson
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,t_rupd) % Coarse grid up to LSM
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])

caxis(collim1hard)
colormap(scale_col3);

% Add smooth grid
hold on
pcolorm(lat_lsm,lon_lsm,flipud(t_int_rupd));
plotm(lsm_rup_proc(2,:), lsm_rup_proc(1,:), 'k-'); % LSM

% tidy up
text(-2.3,4.1, 'a','fontsize',fontsz,'fontname',font)
text(-2.3,1.275, 'b','fontsize',fontsz,'fontname',font)
tightmap
box off
axis off

% Add colorbar
subplot('position', [20 0 1 1])

caxis(collim1hard)
colormap(scale_col3);
color = colorbar('location', 'EastOutside', 'Position', [0.8 0.575 0.022 0.35]);
set(color, 'ytick', min(collim1soft):5:max(collim1soft), 'ylim', collim1soft)
ylabel(color, 'Surface Air Temperature difference (�C)', 'fontname', font, 'fontsize', fontsz)
set(gca, 'fontname', font, 'fontsize', fontsz)

% Freeze colour scheme
freezeColors
cblabel(colorbar);
cbunits(colorbar);
cbfreeze(colorbar);


%% SST Plots

% Priabonian
subplot('position',[0.1 0.255 0.7 0.24])

axesm('MapProjection','Robinson', 'MapLatLim', latlim,... % eqdcylin    Robinson
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,sst_prid) % Coarse grid up to LSM
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])

caxis(collim2hard)
colormap(scale_col3);

% Add smooth grid
hold on
pcolorm(lat_lsm,lon_lsm,flipud(sst_int_prid));
plotm(lsm_pri_proc(2,:), lsm_pri_proc(1,:), 'k-'); % LSM

% tidy up
tightmap
box off
axis off


% Rupelian
subplot('position', [0.1 0.005 0.7 0.24])

axesm('MapProjection','Robinson', 'MapLatLim', latlim,... % eqdcylin    Robinson
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,sst_rupd) % Coarse grid up to LSM
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])

caxis(collim2hard)
colormap(scale_col3);

% Add smooth grid
hold on
pcolorm(lat_lsm,lon_lsm,flipud(sst_int_rupd));
plotm(lsm_rup_proc(2,:), lsm_rup_proc(1,:), 'k-'); % LSM

% tidy up
text(-2.3,4.1, 'c','fontsize',fontsz,'fontname',font)
text(-2.3,1.275, 'd','fontsize',fontsz,'fontname',font)
tightmap
box off
axis off

% Add colorbar
subplot('position', [2 0 1 1])
caxis(collim2hard)
colormap(scale_col3);
color = colorbar('location', 'EastOutside', 'Position', [0.8 0.075 0.022 0.35]);
set(color, 'ytick', min(collim2soft):1:max(collim2soft), 'ylim',collim2soft)
ylabel(color, 'Sea Surface Temperature difference (�C)', 'fontname', font, 'fontsize', fontsz)
set(gca, 'fontname', font, 'fontsize', fontsz)

% final tidy up
truesize([680 510])
set(gcf, 'color', 'w');

