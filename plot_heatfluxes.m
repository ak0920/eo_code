% plot_heatfluxes
% Kennedy et al. 2015, Figure 7

% Load data
load_heatfluxes

% Set fig properties
lw = 2;
font = 'Times New Roman';
fontsz = 10;

close(gcf)
figure


%% Rupelian no ice example
subplot(2,2,1)
plot(hf_rupn(:,1),hf_rupn(:, 2), '-','color',[0.0585574759170413,0.364924505352974,0.679114878177643],'linewidth',lw)
hold on
plot(hf_rupn(:,1),hf_rupn(:, 4), '-','color', [0.850000000000000,0.0872549042105675,0],'linewidth',lw)
plot(hf_rupn(:,1),hf_rupn(:, 3), '-k','linewidth',lw)

% set lims and add zero line
xlim([-90 90])
ylim([-6 6])
plot([90 -90],[0 0], ':', 'color' , [0.6 0.6 0.6])

% tidy up
set(gca, 'fontname', font, 'fontsize', fontsz, 'xtick',[-90:30:90], 'ytick', [-6:1:6])
ylabel('Northward heat flux (PW)')
xlabel('Latitude (�N)')
text(-80,5.25,  'a','fontname', font, 'fontsize', fontsz)


%% Rupelian ice - no ice
subplot(2,2,2)
plot(hf_rupn(:,1),hf_rupi(:,2) - hf_rupn(:, 2), '-','color',[0.0585574759170413,0.364924505352974,0.679114878177643],'linewidth',lw)
hold on
plot(hf_rupn(:,1),hf_rupi(:,4) - hf_rupn(:, 4), '-','color', [0.850000000000000,0.0872549042105675,0],'linewidth',lw)
plot(hf_rupn(:,1),hf_rupi(:,3) - hf_rupn(:, 3), '-k','linewidth',lw)

% set lims and add zero line
xlim([-90 90])
ylim([-0.4 0.4])
plot([90 -90],[0 0], ':', 'color' , [0.6 0.6 0.6])

% tidy up
set(gca, 'fontname', font, 'fontsize', fontsz, 'xtick',[-90:30:90], 'ytick', [-0.4:0.1:0.4])
ylabel('Northward heat flux change (PW)')
xlabel('Latitude (�N)')
text(-80,0.35,  'b','fontname', font, 'fontsize', fontsz)

% legend
leg = legend('Oceanic', 'Atmospheric','Total','location','NorthEast');


%% Priabonian ice - no ice
subplot(2,2,3)
plot(hf_rupn(:,1),hf_priii(:,2) - hf_prin(:, 2), '-','color',[0.0585574759170413,0.364924505352974,0.679114878177643],'linewidth',lw)
hold on
plot(hf_rupn(:,1),hf_priii(:,4) - hf_prin(:, 4), '-','color',[0.850000000000000,0.0872549042105675,0],'linewidth',lw)
plot(hf_rupn(:,1),hf_priii(:,3) - hf_prin(:, 3), '-k','linewidth',lw)

% set lims and add zero line
xlim([-90 90])
ylim([-0.4 0.4])
plot([90 -90],[0 0], ':', 'color' , [0.6 0.6 0.6])

% tidy up
set(gca, 'fontname', font, 'fontsize', fontsz, 'xtick',[-90:30:90], 'ytick', [-0.4:0.1:0.4])
ylabel('Northward heat flux change (PW)')
xlabel('Latitude (�N)')
text(-80,0.35,  'c','fontname', font, 'fontsize', fontsz)


%% Stage diff (Rupelian no ice - Priabonian no ice)
subplot(2,2,4)
plot(hf_rupn(:,1),hf_rupn(:,2) - hf_prin(:, 2), '-','color',[0.0585574759170413,0.364924505352974,0.679114878177643],'linewidth',lw)
hold on
plot(hf_rupn(:,1),hf_rupn(:,4) - hf_prin(:, 4), '-','color',[0.850000000000000,0.0872549042105675,0],'linewidth',lw)
plot(hf_rupn(:,1),hf_rupn(:,3) - hf_prin(:, 3), '-k','linewidth',lw)

% set lims and add zero line
xlim([-90 90])
ylim([-0.4 0.4])
plot([90 -90],[0 0], ':', 'color' , [0.6 0.6 0.6])

% tidy up
set(gca, 'fontname', font, 'fontsize', fontsz, 'xtick',[-90:30:90], 'ytick', [-0.4:0.1:0.4])
ylabel('Northward heat flux change (PW)')
xlabel('Latitude (�N)')
text(-80,0.35,  'd','fontname', font, 'fontsize', fontsz)


% final tidy up
set(gcf, 'position', [0 0 510 400])
