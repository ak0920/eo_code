%% Load original data
test = zeros(96,73);

lsmC = ncread('cha_025.nc', 'bathuk');
lsmR = ncread('rup_025.nc', 'bathuk');
imC = ncread('cha_025.nc', 'landicemaskuk');
imR = ncread('rup_025.nc', 'landicemaskuk');
topoC = ncread('cha_025.nc', 'oroguk');
topoR = ncread('rup_025.nc', 'oroguk');
albC = ncread('cha_025.nc', 'field1395uk');
albR = ncread('rup_025.nc', 'field1395uk');
sdC = ncread('cha_025.nc', 'orogsd1auk');
sdR = ncread('rup_025.nc', 'orogsd1auk');
xxC = ncread('cha_025.nc', 'orogxx1auk');
xxR = ncread('rup_025.nc', 'orogxx1auk');
xyC = ncread('cha_025.nc', 'orogxy1auk');
xyR = ncread('rup_025.nc', 'orogxy1auk');
yyC = ncread('cha_025.nc', 'orogyy1auk');
yyR = ncread('rup_025.nc', 'orogyy1auk');
f329C = ncread('cha_025.nc', 'field329uk');
f329R = ncread('rup_025.nc', 'field329uk');
f330C = ncread('cha_025.nc', 'field330uk');
f330R = ncread('rup_025.nc', 'field330uk');
f331C = ncread('cha_025.nc', 'field331uk');
f331R = ncread('rup_025.nc', 'field331uk');
f332C = ncread('cha_025.nc', 'field332uk');
f332R = ncread('rup_025.nc', 'field332uk');
f333C = ncread('cha_025.nc', 'field333uk');
f333R = ncread('rup_025.nc', 'field333uk');
f335C = ncread('cha_025.nc', 'field335uk');
f335R = ncread('rup_025.nc', 'field335uk');
f336C = ncread('cha_025.nc', 'field336uk');
f336R = ncread('rup_025.nc', 'field336uk');
f342C = ncread('cha_025.nc', 'field342uk');
f342R = ncread('rup_025.nc', 'field342uk');
f1381C = ncread('cha_025.nc', 'field1381uk');
f1381R = ncread('rup_025.nc', 'field1381uk');
f321C = ncread('cha_025.nc', 'field321uk');
f321R = ncread('rup_025.nc', 'field321uk');
f1397C = ncread('cha_025.nc', 'field1397uk');
f1397R = ncread('rup_025.nc', 'field1397uk');
f322C = ncread('cha_025.nc', 'field322uk');
f322R = ncread('rup_025.nc', 'field322uk');
f323C = ncread('cha_025.nc', 'field323uk');
f323R = ncread('rup_025.nc', 'field323uk');
f324C = ncread('cha_025.nc', 'field324uk');
f324R = ncread('rup_025.nc', 'field324uk');
f325C = ncread('cha_025.nc', 'field325uk');
f325R = ncread('rup_025.nc', 'field325uk');
f326C = ncread('cha_025.nc', 'field326uk');
f326R = ncread('rup_025.nc', 'field326uk');
f327C = ncread('cha_025.nc', 'field327uk');
f327R = ncread('rup_025.nc', 'field327uk');
f328C = ncread('cha_025.nc', 'field328uk');
f328R = ncread('rup_025.nc', 'field328uk');
sndC = ncread('cha_025.nc', 'snowdepthuk');
sndR = ncread('rup_025.nc', 'snowdepthuk');
stotC = ncread('cha_025.nc', 'soiltotaluk');
stotR = ncread('rup_025.nc', 'soiltotaluk');
smC = ncread('cha_025.nc', 'soilmoisuk');
smR = ncread('rup_025.nc', 'soilmoisuk');
stC = ncread('cha_025.nc', 'soiltempuk');
stR = ncread('rup_025.nc', 'soiltempuk');
f1391C = ncread('cha_025.nc', 'field1391uk');
f1391R = ncread('rup_025.nc', 'field1391uk');
f1384C = ncread('cha_025.nc', 'field1384uk');
f1384R = ncread('rup_025.nc', 'field1384uk');
f1383C = ncread('cha_025.nc', 'field1383uk');
f1383R = ncread('rup_025.nc', 'field1383uk');
f1382C = ncread('cha_025.nc', 'field1382uk');
f1382R = ncread('rup_025.nc', 'field1382uk');
tfrC = ncread('cha_025.nc', 'totalfracuk');
tfrR = ncread('rup_025.nc', 'totalfracuk');
silC = ncread('cha_025.nc', 'silh1auk');
silR = ncread('rup_025.nc', 'silh1auk');
peakC = ncread('cha_025.nc', 'peak1auk');
peakR = ncread('rup_025.nc', 'peak1auk');




%% Load data again to reconfigure
lsmC2 = ncread('cha_025.nc', 'bathuk');
lsmR2 = ncread('rup_025.nc', 'bathuk');
imC2 = ncread('cha_025.nc', 'landicemaskuk');
imR2 = ncread('rup_025.nc', 'landicemaskuk');
topoC2 = ncread('cha_025.nc', 'oroguk');
topoR2 = ncread('rup_025.nc', 'oroguk');
albC2 = ncread('cha_025.nc', 'field1395uk');
albR2 = ncread('rup_025.nc', 'field1395uk');
sdC2 = ncread('cha_025.nc', 'orogsd1auk');
sdR2 = ncread('rup_025.nc', 'orogsd1auk');
xxC2 = ncread('cha_025.nc', 'orogxx1auk');
xxR2 = ncread('rup_025.nc', 'orogxx1auk');
xyC2 = ncread('cha_025.nc', 'orogxy1auk');
xyR2 = ncread('rup_025.nc', 'orogxy1auk');
yyC2 = ncread('cha_025.nc', 'orogyy1auk');
yyR2 = ncread('rup_025.nc', 'orogyy1auk');
f329C2 = ncread('cha_025.nc', 'field329uk');
f329R2 = ncread('rup_025.nc', 'field329uk');
f330C2 = ncread('cha_025.nc', 'field330uk');
f330R2 = ncread('rup_025.nc', 'field330uk');
f331C2 = ncread('cha_025.nc', 'field331uk');
f331R2 = ncread('rup_025.nc', 'field331uk');
f332C2 = ncread('cha_025.nc', 'field332uk');
f332R2 = ncread('rup_025.nc', 'field332uk');
f333C2 = ncread('cha_025.nc', 'field333uk');
f333R2 = ncread('rup_025.nc', 'field333uk');
f335C2 = ncread('cha_025.nc', 'field335uk');
f335R2 = ncread('rup_025.nc', 'field335uk');
f336C2 = ncread('cha_025.nc', 'field336uk');
f336R2 = ncread('rup_025.nc', 'field336uk');
f342C2 = ncread('cha_025.nc', 'field342uk');
f342R2 = ncread('rup_025.nc', 'field342uk');
f1381C2 = ncread('cha_025.nc', 'field1381uk');
f1381R2 = ncread('rup_025.nc', 'field1381uk');
f321C2 = ncread('cha_025.nc', 'field321uk');
f321R2 = ncread('rup_025.nc', 'field321uk');
f1397C2 = ncread('cha_025.nc', 'field1397uk');
f1397R2 = ncread('rup_025.nc', 'field1397uk');
f322C2 = ncread('cha_025.nc', 'field322uk');
f322R2 = ncread('rup_025.nc', 'field322uk');
f323C2 = ncread('cha_025.nc', 'field323uk');
f323R2 = ncread('rup_025.nc', 'field323uk');
f324C2 = ncread('cha_025.nc', 'field324uk');
f324R2 = ncread('rup_025.nc', 'field324uk');
f325C2 = ncread('cha_025.nc', 'field325uk');
f325R2 = ncread('rup_025.nc', 'field325uk');
f326C2 = ncread('cha_025.nc', 'field326uk');
f326R2 = ncread('rup_025.nc', 'field326uk');
f327C2 = ncread('cha_025.nc', 'field327uk');
f327R2 = ncread('rup_025.nc', 'field327uk');
f328C2 = ncread('cha_025.nc', 'field328uk');
f328R2 = ncread('rup_025.nc', 'field328uk');
sndC2 = ncread('cha_025.nc', 'snowdepthuk');
sndR2 = ncread('rup_025.nc', 'snowdepthuk');
stotC2 = ncread('cha_025.nc', 'soiltotaluk');
stotR2 = ncread('rup_025.nc', 'soiltotaluk');
smC2 = ncread('cha_025.nc', 'soilmoisuk');
smR2 = ncread('rup_025.nc', 'soilmoisuk');
stC2 = ncread('cha_025.nc', 'soiltempuk');
stR2 = ncread('rup_025.nc', 'soiltempuk');
f1391C2 = ncread('cha_025.nc', 'field1391uk');
f1391R2 = ncread('rup_025.nc', 'field1391uk');
f1384C2 = ncread('cha_025.nc', 'field1384uk');
f1384R2 = ncread('rup_025.nc', 'field1384uk');
f1383C2 = ncread('cha_025.nc', 'field1383uk');
f1383R2 = ncread('rup_025.nc', 'field1383uk');
f1382C2 = ncread('cha_025.nc', 'field1382uk');
f1382R2 = ncread('rup_025.nc', 'field1382uk');
tfrC2 = ncread('cha_025.nc', 'totalfracuk');
tfrR2 = ncread('rup_025.nc', 'totalfracuk');
silC2 = ncread('cha_025.nc', 'silh1auk');
silR2 = ncread('rup_025.nc', 'silh1auk');
peakC2 = ncread('cha_025.nc', 'peak1auk');
peakR2 = ncread('rup_025.nc', 'peak1auk');


f700C2 = ncread('cha_040.nc', 'field700uk');
lsmC2 = ncread('cha_040.nc', 'landuk');



%% Loop copying ice sheets between stages

for j = 63:73; % All ice sheet points lie within these columns
    for i = 1:96;
    
        % Chattian
        if topoC(i,j) > 0 && topoR(i,j) > 0               % Only want to change AIS where there is land in both stages (i.e. don't want to change bathymetry)
            imC2(i,j) = imR(i,j);       % Change ice mask
            topoC2(i,j) = topoR(i,j);   % Change topography
            albC2(i,j) = albR(i,j);     % Change albedo (probably not the way to do this - want to change soil cover fractions)
sdC2(i,j) = sdR(i,j);
xxC2(i,j) = xxR(i,j);
xyC2(i,j) = xyR(i,j);
yyC2(i,j) = yyR(i,j);
f329C2(i,j) = f329R(i,j);
f330C2(i,j) = f330R(i,j);
f331C2(i,j) = f331R(i,j);
f332C2(i,j) = f332R(i,j);
f333C2(i,j) = f333R(i,j);
f335C2(i,j) = f335R(i,j);
f336C2(i,j) = f336R(i,j);
f342C2(i,j) = f342R(i,j);
f1381C2(i,j) = f1381R(i,j);
f321C2(i,j) = f321R(i,j);
f1397C2(i,j) = f1397R(i,j);
f322C2(i,j) = f322R(i,j);
f323C2(i,j) = f323R(i,j);
f324C2(i,j) = f324R(i,j);
f325C2(i,j) = f325R(i,j);
f326C2(i,j) = f326R(i,j);
f327C2(i,j) = f327R(i,j);
f328C2(i,j) = f328R(i,j);
f1384C2(i,j) = f1384R(i,j);
f1383C2(i,j) = f1383R(i,j);
f1382C2(i,j) = f1382R(i,j);
tfrC2(i,j) = tfrR(i,j);
silC2(i,j) = silR(i,j);
peakC2(i,j) = peakR(i,j);

for k = 1:4; 
    for l = 1:12;
        for n = 1:73;
sndC2(i,j,l) = sndR(i,j,l);
stotC2(i,j,l) = stotR(i,j,l);
smC2(i,j,k,l) = smR(i,j,k,l);
stC2(i,j,k,l) = stR(i,j,k,l);
% Fix wacky snow values
if sndC2(i,n,l) ~= 5e4 && sndC2(i,n,l) > 0
sndC2(i,n,l) = 0;
end
        end
    end;end;
for m = 1:9;
    
f1391C2(i,j,m) = f1391R(i,j,m);
end


% Remove excess land point
imC2(90,68) = imC2(90,60);     
topoC2(90,68) = topoC2(90,60);
albC2(90,68) = albC2(90,60);
sdC2(90,68) = sdC2(90,60);
xxC2(90,68) = xxC2(90,60);
xyC2(90,68) = xyC2(90,60);
yyC2(90,68) = yyC2(90,60);
f329C2(90,68) = f329C2(90,60);
f330C2(90,68) = f330C2(90,60);
f331C2(90,68) = f331C2(90,60);
f332C2(90,68) = f332C2(90,60);
f333C2(90,68) = f333C2(90,60);
f335C2(90,68) = f335C2(90,60);
f336C2(90,68) = f336C2(90,60);
f342C2(90,68) = f342C2(90,60);
f1381C2(90,68) = f1381C2(90,60);
f321C2(90,68) = f321C2(90,60);
f1397C2(90,68) = f1397C2(90,60);
f322C2(90,68) = f322C2(90,60);
f323C2(90,68) = f323C2(90,60);
f324C2(90,68) = f324C2(90,60);
f325C2(90,68) = f325C2(90,60);
f326C2(90,68) = f326C2(90,60);
f327C2(90,68) = f327C2(90,60);
f328C2(90,68) = f328C2(90,60);
f1384C2(90,68) = f1384C2(90,60);
f1383C2(90,68) = f1383C2(90,60);
f1382C2(90,68) = f1382C2(90,60);
tfrC2(90,68) = tfrC2(90,60);
silC2(90,68) = silC2(90,60);
peakC2(90,68) = peakC2(90,60);

lsmC2(90,68) = lsmC2(90,60);
f700C2(90,68) = f700C2(90,60);

for k = 1:4; 
    for l = 1:12;
        for n = 1:73;
sndC2(90,68,l) = sndC2(90,60,l);
stotC2(90,68,l) = stotC2(90,60,l);
smC2(90,68,k,l) = smC2(90,60,k,l);
stC2(90,68,k,l) = stC2(90,60,k,l);

        end
    end;
end;
for m = 1:9;
    
f1391C2(90,68,m) = f1391C2(90,60,m);
end





        end
        % Rupelian
        if topoR(i,j) > 0 && topoC(i,j) > 0
            test(i,j) = 1;
            imR2(i,j) = imC(i,j);
            topoR2(i,j) = topoC(i,j);
            albR2(i,j) = albC(i,j);

% imC2(i,j) = imR(i,j);
% topoC2(i,j) = topoR(i,j);
% albC2(i,j) = albR(i,j);
sdR2(i,j) = sdC(i,j);
xxR2(i,j) = xxC(i,j);
xyR2(i,j) = xyC(i,j);
yyR2(i,j) = yyC(i,j);
f329R2(i,j) = f329C(i,j);
f330R2(i,j) = f330C(i,j);
f331R2(i,j) = f331C(i,j);
f332R2(i,j) = f332C(i,j);
f333R2(i,j) = f333C(i,j);
f335R2(i,j) = f335C(i,j);
f336R2(i,j) = f336C(i,j);
f342R2(i,j) = f342C(i,j);
f1381R2(i,j) = f1381C(i,j);
f321R2(i,j) = f321C(i,j);
f1397R2(i,j) = f1397C(i,j);
f322R2(i,j) = f322C(i,j);
f323R2(i,j) = f323C(i,j);
f324R2(i,j) = f324C(i,j);
f325R2(i,j) = f325C(i,j);
f326R2(i,j) = f326C(i,j);
f327R2(i,j) = f327C(i,j);
f328R2(i,j) = f328C(i,j);
f1384R2(i,j) = f1384C(i,j);
f1383R2(i,j) = f1383C(i,j);
f1382R2(i,j) = f1382C(i,j);
tfrR2(i,j) = tfrC(i,j);
silR2(i,j) = silC(i,j);
peakR2(i,j) = peakC(i,j);

for k = 1:4;
    for l = 1:12;
sndR2(i,j,l) = sndC(i,j,l);
stotR2(i,j,l) = stotC(i,j,l);
smR2(i,j,k,l) = smC(i,j,k,l);
stR2(i,j,k,l) = stC(i,j,k,l);

% Fix wacky snow values
if sndR2(i,j,l) ~= 5e4 && sndR2(i,j,l) >0
sndR2(i,j,l) = 0;
end
    

    end
end
for m = 1:9;
f1391R2(i,j,m) = f1391C(i,j,m);
end

        end
    end
end






%% Write new output as netCDF

ncwrite('cha_040.nc', 'landicemaskuk', imC2);
ncwrite('rup_040.nc', 'landicemaskuk', imR2);
ncwrite('cha_040.nc', 'oroguk', topoC2);
ncwrite('rup_040.nc', 'oroguk', topoR2);
ncwrite('cha_040.nc', 'field1395uk', albC2);
ncwrite('rup_040.nc', 'field1395uk', albR2);
ncwrite('cha_040.nc', 'orogsd1auk', sdC2);
ncwrite('rup_040.nc', 'orogsd1auk', sdR2);
ncwrite('cha_040.nc', 'orogxx1auk', xxC2);
ncwrite('rup_040.nc', 'orogxx1auk', xxR2);
ncwrite('cha_040.nc', 'orogxy1auk', xyC2);
ncwrite('rup_040.nc', 'orogxy1auk', xyR2);
ncwrite('cha_040.nc', 'orogyy1auk', yyC2);
ncwrite('rup_040.nc', 'orogyy1auk', yyR2);
ncwrite('cha_040.nc', 'field329uk', f329C2);
ncwrite('rup_040.nc', 'field329uk', f329R2);
ncwrite('cha_040.nc', 'field330uk', f330C2);
ncwrite('rup_040.nc', 'field330uk', f330R2);
ncwrite('cha_040.nc', 'field331uk', f331C2);
ncwrite('rup_040.nc', 'field331uk', f331R2);
ncwrite('cha_040.nc', 'field332uk', f332C2);
ncwrite('rup_040.nc', 'field332uk', f332R2);
ncwrite('cha_040.nc', 'field333uk', f333C2);
ncwrite('rup_040.nc', 'field333uk', f333R2);
ncwrite('cha_040.nc', 'field335uk', f335C2);
ncwrite('rup_040.nc', 'field335uk', f335R2);
ncwrite('cha_040.nc', 'field336uk', f336C2);
ncwrite('rup_040.nc', 'field336uk', f336R2);
ncwrite('cha_040.nc', 'field342uk', f342C2);
ncwrite('rup_040.nc', 'field342uk', f342R2);
ncwrite('cha_040.nc', 'field321uk', f321C2);
ncwrite('rup_040.nc', 'field321uk', f321R2);
ncwrite('cha_040.nc', 'field1381uk', f1381C2);
ncwrite('rup_040.nc', 'field1381uk', f1381R2);
ncwrite('cha_040.nc', 'field1397uk', f1397C2);
ncwrite('rup_040.nc', 'field1397uk', f1397R2);
ncwrite('cha_040.nc', 'field322uk', f322C2);
ncwrite('rup_040.nc', 'field322uk', f322R2);
ncwrite('cha_040.nc', 'field323uk', f323C2);
ncwrite('rup_040.nc', 'field323uk', f323R2);
ncwrite('cha_040.nc', 'field324uk', f324C2);
ncwrite('rup_040.nc', 'field324uk', f324R2);
ncwrite('cha_040.nc', 'field325uk', f325C2);
ncwrite('rup_040.nc', 'field325uk', f325R2);
ncwrite('cha_040.nc', 'field326uk', f326C2);
ncwrite('rup_040.nc', 'field326uk', f326R2);
ncwrite('cha_040.nc', 'field327uk', f327C2);
ncwrite('rup_040.nc', 'field327uk', f327R2);
ncwrite('cha_040.nc', 'field328uk', f328C2);
ncwrite('rup_040.nc', 'field328uk', f328R2);
ncwrite('cha_040.nc', 'snowdepthuk', sndC2);
ncwrite('rup_040.nc', 'snowdepthuk', sndR2);
ncwrite('cha_040.nc', 'soiltotaluk', stotC2);
ncwrite('rup_040.nc', 'soiltotaluk', stotR2);
ncwrite('cha_040.nc', 'soilmoisuk', smC2);
ncwrite('rup_040.nc', 'soilmoisuk', smR2);
ncwrite('cha_040.nc', 'soiltempuk', stC2);
ncwrite('rup_040.nc', 'soiltempuk', stR2);

ncwrite('cha_040.nc', 'field1391uk', f1391C2);
ncwrite('rup_040.nc', 'field1391uk', f1391R2);
ncwrite('cha_040.nc', 'field1384uk', f1384C2);
ncwrite('rup_040.nc', 'field1384uk', f1384R2);
ncwrite('cha_040.nc', 'field1383uk', f1383C2);
ncwrite('rup_040.nc', 'field1383uk', f1383R2);
ncwrite('cha_040.nc', 'field1382uk', f1382C2);
ncwrite('rup_040.nc', 'field1382uk', f1382R2);
ncwrite('cha_040.nc', 'totalfracuk', tfrC2);
ncwrite('rup_040.nc', 'totalfracuk', tfrR2);
ncwrite('cha_040.nc', 'silh1auk', silC2);
ncwrite('rup_040.nc', 'silh1auk', silR2);
ncwrite('cha_040.nc', 'peak1auk', peakC2);
ncwrite('rup_040.nc', 'peak1auk', peakR2);
