% load_lsm

% Load a SST field to get LSM and ice mask from ancil file
sst_prin = rot90(ncread('../EO_data/tdwqjo.pfclann.nc', 'temp_mm_uo'));
sst_rupn = rot90(ncread('../EO_data/tdluto.pfclann.nc', 'temp_mm_uo'));
sst_chan = rot90(ncread('../EO_data/tdluuo.pfclann.nc', 'temp_mm_uo'));
imr      = rot90(ncread('ancil/rup_025.nc', 'landicemaskuk'));
imc      = rot90(ncread('ancil/cha_025.nc', 'landicemaskuk'));
imp      = rot90(ncread('ancil/pri_025.nc', 'landicemaskuk'));

% Process into useable data for plots
lsm_rup_proc = proc_lsm(flipud(isnan(sst_rupn)*1)); % lsm_proc(lsm_rup);
lsm_cha_proc = proc_lsm(flipud(isnan(sst_chan)*1)); % lsm_proc(lsm_cha);
lsm_pri_proc = proc_lsm(flipud(isnan(sst_prin)*1)); % lsm_proc(lsm_cha);
im_rup_proc  = proc_lsm(imr); % lsm_proc(lsm_rup);
im_cha_proc  = proc_lsm(imc); % lsm_proc(lsm_cha);
