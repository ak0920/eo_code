% load_hill_data
% 
% There was some confusion as to which exact simulations were used in the
% Hill et al. 2013 paper. This script allows the chosen simulations to be
% changed easily.
% 


% Select job descriptions: tboy__
correct_noice = 'h1'; 
correct_ice = 'q1';

% generate grids to mesh/interpolate between
load_latlon
[low_resX, low_resY] = meshgrid(lon,lat);
[hi_resX, hi_resY]   = meshgrid(lon_lsm, lat_lsm);


%% stream function
str_rupn_h = rot90(ncread(strcat('../EO_data/tboy',correct_noice,'o.pfclann.nc'), 'streamFn_mm_uo'))/1e12;
str_rupi_h = rot90(ncread(strcat('../EO_data/tboy',correct_ice,'o.pfclann.nc'), 'streamFn_mm_uo'))/1e12;


%% SAT
t_rupnh = flipud(rot90(ncread(strcat('../EO_data/tboy',correct_noice,'a.pdclann.nc'), 'temp_mm_1_5m')));
t_rupih = flipud(rot90(ncread(strcat('../EO_data/tboy',correct_ice,'a.pdclann.nc'), 'temp_mm_1_5m')));


%% SST
sst_rupnh = rot90(ncread(strcat('../EO_data/tboy',correct_noice,'o.pfclann.nc'), 'temp_mm_uo'));
sst_rupih = rot90(ncread(strcat('../EO_data/tboy',correct_ice,'o.pfclann.nc'), 'temp_mm_uo'));


%% Pressure
P_rupnh = ncread(strcat('../EO_data/tboy',correct_noice,'a.pcclann.nc'), 'ht_mm_p');
P_rupih = ncread(strcat('../EO_data/tboy',correct_ice,'a.pcclann.nc'), 'ht_mm_p');

% Select 500 hPa level
p850_rupnh = flipud(rot90(P_rupnh(:,:,6)));
p850_rupih = flipud(rot90(P_rupih(:,:,6)));

% interpolate
P_int_rupnh = interp2(low_resX,low_resY,p850_rupnh,hi_resX,hi_resY);
P_int_rupih = interp2(low_resX,low_resY,p850_rupih,hi_resX,hi_resY);


%% Wind

agg = 3; % also used in surface currents

% Load U and V components
u_rupnh = rot90(ncread(strcat('../EO_data/tboy',correct_noice,'a.pdclann.nc'), 'u_mm_10m'));
u_rupih = rot90(ncread(strcat('../EO_data/tboy',correct_ice,'a.pdclann.nc'), 'u_mm_10m'));
v_rupnh = rot90(ncread(strcat('../EO_data/tboy',correct_noice,'a.pdclann.nc'), 'v_mm_10m'));
v_rupih = rot90(ncread(strcat('../EO_data/tboy',correct_ice,'a.pdclann.nc'), 'v_mm_10m'));

% add row of zeros to fit on grid
u_rupnh(73,:) = 0;
u_rupih(73,:) = 0;
v_rupnh(73,:) = 0;
v_rupih(73,:) = 0;

% aggregate together, as in load_winds
[windu_rupnh, windv_rupnh]             = proc_ave_winds(u_rupnh, v_rupnh, agg);
[windu_rupih, windv_rupih,windX,windY] = proc_ave_winds(u_rupih, v_rupih, agg);


%% Surface currents

% as above, but top layer of ocean also needs selected
uc_rupnh = ncread(strcat('../EO_data/tboy',correct_noice,'o.pgclann.nc'), 'ucurrTot_ym_dpth');
uc_rupih = ncread(strcat('../EO_data/tboy',correct_ice,'o.pgclann.nc'), 'ucurrTot_ym_dpth');
vc_rupnh = ncread(strcat('../EO_data/tboy',correct_noice,'o.pgclann.nc'), 'vcurrTot_ym_dpth');
vc_rupih = ncread(strcat('../EO_data/tboy',correct_ice,'o.pgclann.nc'), 'vcurrTot_ym_dpth');

uc_rupnh = flipud(rot90(uc_rupnh(:,:,1)));
uc_rupih = flipud(rot90(uc_rupih(:,:,1)));
vc_rupnh = flipud(rot90(vc_rupnh(:,:,1)));
vc_rupih = flipud(rot90(vc_rupih(:,:,1)));

uc_rupnh(73,:) = 0;
uc_rupih(73,:) = 0;
vc_rupnh(73,:) = 0;
vc_rupih(73,:) = 0;

[curu_rupnh, curv_rupnh] = proc_ave_winds(uc_rupnh, vc_rupnh);
[curu_rupih, curv_rupih] = proc_ave_winds(uc_rupih, vc_rupih);




