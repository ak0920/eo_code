%% Plot the stream function
% Kennedy et al. 2015, Figure 5

% Load data
load_strfn
load_lsm
load col_scale3.mat % aka scale_col3

% Some other variables/set up things
font      = 'Times New Roman';
fontsz    = 10;
col_range = [-200 200];  % this is greater than it needs to be to make the colours lighter
land_val  = 205;         % Land value, must be outside range of cax to be grey
con_lev   = -200:20:200; % contour levels

% Select manual or default placing of contours:
% con = 'manual';
con = []; % default

figure


%% Priabonian no ice
subplot('position', [0.1 0.755 0.7 0.24])

imagesc(str_prin(7:70,:))
caxis(col_range)
colormap(scale_col3);

% grey out land
hold on
lrn = imagesc(~isnan(sst_prin(7:70,:))-land_val);
set(lrn, 'AlphaData', isnan(sst_prin(7:70,:)))

% add contours for strfn and coastline
[c1,h1] = contour(str_prin(7:70,:),con_lev,'k');
clabel(c1, h1, con,'fontname',font)
plot(lsm_pri_proc(1,:)/3.75+0.5,lsm_pri_proc(2,:)/-2.5+37-6,'k')

% Add gridlines
plot([1 96], [55 55], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [31 31], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [7 7],   ':', 'color', [0.3 0.3 0.3])
plot([17 17], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([33 33], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([49 49], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([65 65], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([81 81], [1 73], ':', 'color', [0.3 0.3 0.3])

% tidy up
box on
axis off
text(-2.3,5, 'a','fontsize',fontsz,'fontname',font)


%% Priabonian ice
subplot('position', [0.1 0.505 0.7 0.24])

imagesc(str_priii(7:70,:))
caxis(col_range)
colormap(scale_col3);

% grey out land
hold on
lri = imagesc(~isnan(sst_prin(7:70,:))-land_val);
set(lri, 'AlphaData', isnan(sst_prin(7:70,:)))

% add contours for strfn and coastline
[c2,h2] = contour(str_priii(7:70,:),con_lev,'k');
clabel(c2, h2, con,'fontname',font)
plot(lsm_pri_proc(1,:)/3.75+0.5,lsm_pri_proc(2,:)/-2.5+37-6,'k')

% Add gridlines
plot([1 96], [55 55], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [31 31], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [7 7],   ':', 'color', [0.3 0.3 0.3])
plot([17 17], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([33 33], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([49 49], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([65 65], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([81 81], [1 73], ':', 'color', [0.3 0.3 0.3])

% tidy up
box on
axis off
text(-2.3,5, 'b','fontsize',fontsz,'fontname',font)


%% Rupelian no ice
subplot('position', [0.1 0.255 0.7 0.24])

imagesc(str_rupn(7:70,:))
caxis(col_range)
colormap(scale_col3);

% grey out land
hold on
lcn = imagesc(~isnan(sst_rupn(7:70,:))-land_val);
set(lcn, 'AlphaData', isnan(sst_rupn(7:70,:)))

% add contours for strfn and coastline
[c3,h3] = contour(str_rupn(7:70,:),con_lev,'k');
clabel(c3, h3, con,'fontname',font)
plot(lsm_rup_proc(1,:)/3.75+0.5,lsm_rup_proc(2,:)/-2.5+37-6,'k')

% Add gridlines
plot([1 96], [55 55], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [31 31], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [7 7],   ':', 'color', [0.3 0.3 0.3])
plot([17 17], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([33 33], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([49 49], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([65 65], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([81 81], [1 73], ':', 'color', [0.3 0.3 0.3])

% tidy up
axis off
box on
text(-2.3,5, 'c','fontsize',fontsz,'fontname',font)


%% Rupelian ice
subplot('position', [0.1 0.005 0.7 0.24])

imagesc(str_rupii(7:70,:))
caxis(col_range)
colormap(scale_col3);

% grey out land
hold on
lci = imagesc(~isnan(sst_rupn(7:70,:))-land_val);
set(lci, 'AlphaData', isnan(sst_rupn(7:70,:)))

% add contours for strfn and coastline
[c4,h4] = contour(str_rupii(7:70,:),con_lev,'k');
clabel(c4,h4, con,'fontname',font)
plot(lsm_rup_proc(1,:)/3.75+0.5,lsm_rup_proc(2,:)/-2.5+37-6,'k')

% Add gridlines
plot([1 96], [55 55], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [31 31], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [7 7],   ':', 'color', [0.3 0.3 0.3])
plot([17 17], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([33 33], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([49 49], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([65 65], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([81 81], [1 73], ':', 'color', [0.3 0.3 0.3])

% tidy up
box on
axis off
text(-2.3,5, 'd','fontsize',fontsz,'fontname',font)


%% Colour bar
subplot('position', [2 0 1 1])
caxis(col_range)
colormap(scale_col3);
color = colorbar('location', 'EastOutside', 'Position', [0.85 0.25 0.022 0.5]);
set(color, 'ytick', [-150:30:150], 'ylim',[-150 150]) % this range is probably bigger than is necessary
ylabel(color, 'Barotropic stream function (Sv)', 'fontname', font, 'fontsize', fontsz)
set(gca, 'fontname', font, 'fontsize', fontsz)

% final tidy up
truesize([680 510])
set(gcf, 'color', 'w');

