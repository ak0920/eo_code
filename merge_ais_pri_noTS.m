% merge_ais_pri.m
%
% This script simply reads in the variables from the Getech boundary
% conditions netCDF file for the Priabonian, Rupelian and Chattian and
% merges the appropriate variables that need modified over the Antarctic
% land area for ice sheet sensitivity studies. Both Rupelian and Chattian
% ice sheets are 'added to the Priabonian palaeogeography and the new ice
% sheet configurations saved as new netCDF files.
%
% NOTE: MAKE A COPY OF THE ORIGINAL NETCDF FILE WITH THE NAME YOU WANT TO
% CALL THE NEW FILE BEFORE RUNNING, E.G. cp pri_050.nc pri_051.nc
% It shouldn't mess anything up if you don't, but some variables are
% unmodified and will be missing from the new netCDF file.


%% Load Rupelian and Chattian data

addpath('../Getech/ancil/pri/', '../Getech/ancil/cha/', '../Getech/ancil/rup/')

test = zeros(96,73);

lsmC = ncread('cha_025.nc', 'bathuk');
lsmR = ncread('rup_025.nc', 'bathuk');
imC = ncread('cha_025.nc', 'landicemaskuk');
imR = ncread('rup_025.nc', 'landicemaskuk');
topoC = ncread('cha_025.nc', 'oroguk');
topoR = ncread('rup_025.nc', 'oroguk');
albC = ncread('cha_025.nc', 'field1395uk');
albR = ncread('rup_025.nc', 'field1395uk');
sdC = ncread('cha_025.nc', 'orogsd1auk');
sdR = ncread('rup_025.nc', 'orogsd1auk');
xxC = ncread('cha_025.nc', 'orogxx1auk');
xxR = ncread('rup_025.nc', 'orogxx1auk');
xyC = ncread('cha_025.nc', 'orogxy1auk');
xyR = ncread('rup_025.nc', 'orogxy1auk');
yyC = ncread('cha_025.nc', 'orogyy1auk');
yyR = ncread('rup_025.nc', 'orogyy1auk');
f329C = ncread('cha_025.nc', 'field329uk');
f329R = ncread('rup_025.nc', 'field329uk');
f330C = ncread('cha_025.nc', 'field330uk');
f330R = ncread('rup_025.nc', 'field330uk');
f331C = ncread('cha_025.nc', 'field331uk');
f331R = ncread('rup_025.nc', 'field331uk');
f332C = ncread('cha_025.nc', 'field332uk');
f332R = ncread('rup_025.nc', 'field332uk');
f333C = ncread('cha_025.nc', 'field333uk');
f333R = ncread('rup_025.nc', 'field333uk');
f335C = ncread('cha_025.nc', 'field335uk');
f335R = ncread('rup_025.nc', 'field335uk');
f336C = ncread('cha_025.nc', 'field336uk');
f336R = ncread('rup_025.nc', 'field336uk');
f342C = ncread('cha_025.nc', 'field342uk');
f342R = ncread('rup_025.nc', 'field342uk');
f1381C = ncread('cha_025.nc', 'field1381uk');
f1381R = ncread('rup_025.nc', 'field1381uk');
f321C = ncread('cha_025.nc', 'field321uk');
f321R = ncread('rup_025.nc', 'field321uk');
f1397C = ncread('cha_025.nc', 'field1397uk');
f1397R = ncread('rup_025.nc', 'field1397uk');
f322C = ncread('cha_025.nc', 'field322uk');
f322R = ncread('rup_025.nc', 'field322uk');
f323C = ncread('cha_025.nc', 'field323uk');
f323R = ncread('rup_025.nc', 'field323uk');
f324C = ncread('cha_025.nc', 'field324uk');
f324R = ncread('rup_025.nc', 'field324uk');
f325C = ncread('cha_025.nc', 'field325uk');
f325R = ncread('rup_025.nc', 'field325uk');
f326C = ncread('cha_025.nc', 'field326uk');
f326R = ncread('rup_025.nc', 'field326uk');
f327C = ncread('cha_025.nc', 'field327uk');
f327R = ncread('rup_025.nc', 'field327uk');
f328C = ncread('cha_025.nc', 'field328uk');
f328R = ncread('rup_025.nc', 'field328uk');
sndC = ncread('cha_025.nc', 'snowdepthuk');
sndR = ncread('rup_025.nc', 'snowdepthuk');
stotC = ncread('cha_025.nc', 'soiltotaluk');
stotR = ncread('rup_025.nc', 'soiltotaluk');
smC = ncread('cha_025.nc', 'soilmoisuk');
smR = ncread('rup_025.nc', 'soilmoisuk');
stC = ncread('cha_025.nc', 'soiltempuk');
stR = ncread('rup_025.nc', 'soiltempuk');
f1391C = ncread('cha_025.nc', 'field1391uk');
f1391R = ncread('rup_025.nc', 'field1391uk');
f1384C = ncread('cha_025.nc', 'field1384uk');
f1384R = ncread('rup_025.nc', 'field1384uk');
f1383C = ncread('cha_025.nc', 'field1383uk');
f1383R = ncread('rup_025.nc', 'field1383uk');
f1382C = ncread('cha_025.nc', 'field1382uk');
f1382R = ncread('rup_025.nc', 'field1382uk');
tfrC = ncread('cha_025.nc', 'totalfracuk');
tfrR = ncread('rup_025.nc', 'totalfracuk');
silC = ncread('cha_025.nc', 'silh1auk');
silR = ncread('rup_025.nc', 'silh1auk');
peakC = ncread('cha_025.nc', 'peak1auk');
peakR = ncread('rup_025.nc', 'peak1auk');


%% Load Priabonian data to be over-written

lsmP2 = ncread('pri_050.nc', 'bathuk');
lsmP3 = ncread('pri_050.nc', 'bathuk');
imP2 = ncread('pri_025.nc', 'landicemaskuk'); % these have not been produced by libancil
imP3 = ncread('pri_025.nc', 'landicemaskuk'); % these have not been produced by libancil
topoP2 = ncread('pri_050.nc', 'oroguk');
topoP3 = ncread('pri_050.nc', 'oroguk');
albP2 = ncread('pri_050.nc', 'field1395uk');
albP3 = ncread('pri_050.nc', 'field1395uk');
sdP2 = ncread('pri_050.nc', 'orogsd1auk');
sdP3 = ncread('pri_050.nc', 'orogsd1auk');
xxP2 = ncread('pri_050.nc', 'orogxx1auk');
xxP3 = ncread('pri_050.nc', 'orogxx1auk');
xyP2 = ncread('pri_050.nc', 'orogxy1auk');
xyP3 = ncread('pri_050.nc', 'orogxy1auk');
yyP2 = ncread('pri_050.nc', 'orogyy1auk');
yyP3 = ncread('pri_050.nc', 'orogyy1auk');
f329P2 = ncread('pri_050.nc', 'field329uk');
f329P3 = ncread('pri_050.nc', 'field329uk');
f330P2 = ncread('pri_050.nc', 'field330uk');
f330P3 = ncread('pri_050.nc', 'field330uk');
f331P2 = ncread('pri_050.nc', 'field331uk');
f331P3 = ncread('pri_050.nc', 'field331uk');
f332P2 = ncread('pri_050.nc', 'field332uk');
f332P3 = ncread('pri_050.nc', 'field332uk');
f333P2 = ncread('pri_050.nc', 'field333uk');
f333P3 = ncread('pri_050.nc', 'field333uk');
f335P2 = ncread('pri_050.nc', 'field335uk');
f335P3 = ncread('pri_050.nc', 'field335uk');
f336P2 = ncread('pri_050.nc', 'field336uk');
f336P3 = ncread('pri_050.nc', 'field336uk');
f342P2 = ncread('pri_050.nc', 'field342uk');
f342P3 = ncread('pri_050.nc', 'field342uk');
f1381P2 = ncread('pri_050.nc', 'field1381uk');
f1381P3 = ncread('pri_050.nc', 'field1381uk');
f321P2 = ncread('pri_050.nc', 'field321uk');
f321P3 = ncread('pri_050.nc', 'field321uk');
f1397P2 = ncread('pri_050.nc', 'field1397uk');
f1397P3 = ncread('pri_050.nc', 'field1397uk');
f322P2 = ncread('pri_050.nc', 'field322uk');
f322P3 = ncread('pri_050.nc', 'field322uk');
f323P2 = ncread('pri_050.nc', 'field323uk');
f323P3 = ncread('pri_050.nc', 'field323uk');
f324P2 = ncread('pri_050.nc', 'field324uk');
f324P3 = ncread('pri_050.nc', 'field324uk');
f325P2 = ncread('pri_050.nc', 'field325uk');
f325P3 = ncread('pri_050.nc', 'field325uk');
f326P2 = ncread('pri_050.nc', 'field326uk');
f326P3 = ncread('pri_050.nc', 'field326uk');
f327P2 = ncread('pri_050.nc', 'field327uk');
f327P3 = ncread('pri_050.nc', 'field327uk');
f328P2 = ncread('pri_050.nc', 'field328uk');
f328P3 = ncread('pri_050.nc', 'field328uk');
sndP2 = ncread('pri_050.nc', 'snowdepthuk');
sndP3 = ncread('pri_050.nc', 'snowdepthuk');
stotP2 = ncread('pri_050.nc', 'soiltotaluk');
stotP3 = ncread('pri_050.nc', 'soiltotaluk');
smP2 = ncread('pri_050.nc', 'soilmoisuk');
smP3 = ncread('pri_050.nc', 'soilmoisuk');
stP2 = ncread('pri_050.nc', 'soiltempuk');
stP3 = ncread('pri_050.nc', 'soiltempuk');
f1391P2 = ncread('pri_050.nc', 'field1391uk');
f1391P3 = ncread('pri_050.nc', 'field1391uk');
f1384P2 = ncread('pri_050.nc', 'field1384uk');
f1384P3 = ncread('pri_050.nc', 'field1384uk');
f1383P2 = ncread('pri_050.nc', 'field1383uk');
f1383P3 = ncread('pri_050.nc', 'field1383uk');
f1382P2 = ncread('pri_050.nc', 'field1382uk');
f1382P3 = ncread('pri_050.nc', 'field1382uk');
tfrP2 = ncread('pri_050.nc', 'totalfracuk');
tfrP3 = ncread('pri_050.nc', 'totalfracuk');
silP2 = ncread('pri_050.nc', 'silh1auk');
silP3 = ncread('pri_050.nc', 'silh1auk');
peakP2 = ncread('pri_050.nc', 'peak1auk');
peakP3 = ncread('pri_050.nc', 'peak1auk');


%% Loop copying ice sheets between stages

for j = 63:73;                          % All ice sheet points lie within these latitude columns
    for i = 1:96;                       % All longitudes should be used
        
        % Superimpose Rupelian Ice
        if topoC(i,j) > 0 && topoR(i,j) > 0  % Only want to change AIS where there is land in both stages (i.e. don't want to change bathymetry)
            
            imP2(i,j) = imR(i,j);       % ice mask
            topoP2(i,j) = topoR(i,j);   % topography
            albP2(i,j) = albR(i,j);     % soil albedo (field 1395)
            sdP2(i,j) = sdR(i,j);       % sub-grid scale standard deviation (1a method)
            xxP2(i,j) = xxR(i,j);       % sub-grid scale variance xx direction (1a method)
            xyP2(i,j) = xyR(i,j);       % sub-grid scale variance xy direction (1a method)
            yyP2(i,j) = yyR(i,j);       % sub-grid scale variance yy direction (1a method)
            f329P2(i,j) = f329R(i,j);   % wilting point
            f330P2(i,j) = f330R(i,j);   % critical point
            f331P2(i,j) = f331R(i,j);   % field capacity
            f332P2(i,j) = f332R(i,j);   % saturation
            f333P2(i,j) = f333R(i,j);   % sat cond
            f335P2(i,j) = f335R(i,j);   % heat capacity
            f336P2(i,j) = f336R(i,j);   % thermal cond
            f342P2(i,j) = f342R(i,j);   % soil suction
            f1381P2(i,j) = f1381R(i,j); % Clapp-Hornberg
            f321P2(i,j) = f321R(i,j);   % root depth
            f1397P2(i,j) = f1397R(i,j); % soil carbon
            f322P2(i,j) = f322R(i,j);   % snow free albedo
            f323P2(i,j) = f323R(i,j);   % surf resistance
            f324P2(i,j) = f324R(i,j);   % surf roughness
            f325P2(i,j) = f325R(i,j);   % surf capacity
            f326P2(i,j) = f326R(i,j);   % veg frac
            f327P2(i,j) = f327R(i,j);   % veg infilt
            f328P2(i,j) = f328R(i,j);   % deep snow albedo
            f1384P2(i,j) = f1384R(i,j); % can conduct
            f1383P2(i,j) = f1383R(i,j); % canopy ht
            f1382P2(i,j) = f1382R(i,j); % LAI
            tfrP2(i,j) = tfrR(i,j);     % total fraction
            silP2(i,j) = silR(i,j);     % Silhouette orog (1a method)
            peakP2(i,j) = peakR(i,j);   % Peak height (1a method)
            
            for k = 1:4;                % some variables have multiple dimensions needing changed
                for l = 1:12;
                    for n = 1:73;
                        sndP2(i,j,l) = sndR(i,j,l);     % snow depth
                        stotP2(i,j,l) = stotR(i,j,l);   % total soil moisture
                        smP2(i,j,k,l) = smR(i,j,k,l);   % soil moisture
                        stP2(i,j,k,l) = stR(i,j,k,l);   % soil temperature
                        % Fix wacky snow values
                        % if sndP2(i,n,l) ~= 5e4 && sndP2(i,n,l) > 0
                        % sndP2(i,n,l) = 0;
                        % end
                    end;
                end;
            end;
            
            for m = 1:9;
                f1391P2(i,j,m) = f1391R(i,j,m); % frac veg
            end
            
        end
        
        
        
        % superimpose Chattian ice
        if topoR(i,j) > 0 && topoC(i,j) > 0
            test(i,j) = 1;
            imP3(i,j) = imC(i,j);
            topoP3(i,j) = topoC(i,j);
            albP3(i,j) = albC(i,j);
            
            % imC2(i,j) = imR(i,j);
            % topoC2(i,j) = topoR(i,j);
            % albC2(i,j) = albR(i,j);
            sdP3(i,j) = sdC(i,j);
            xxP3(i,j) = xxC(i,j);
            xyP3(i,j) = xyC(i,j);
            yyP3(i,j) = yyC(i,j);
            f329P3(i,j) = f329C(i,j);
            f330P3(i,j) = f330C(i,j);
            f331P3(i,j) = f331C(i,j);
            f332P3(i,j) = f332C(i,j);
            f333P3(i,j) = f333C(i,j);
            f335P3(i,j) = f335C(i,j);
            f336P3(i,j) = f336C(i,j);
            f342P3(i,j) = f342C(i,j);
            f1381P3(i,j) = f1381C(i,j);
            f321P3(i,j) = f321C(i,j);
            f1397P3(i,j) = f1397C(i,j);
            f322P3(i,j) = f322C(i,j);
            f323P3(i,j) = f323C(i,j);
            f324P3(i,j) = f324C(i,j);
            f325P3(i,j) = f325C(i,j);
            f326P3(i,j) = f326C(i,j);
            f327P3(i,j) = f327C(i,j);
            f328P3(i,j) = f328C(i,j);
            f1384P3(i,j) = f1384C(i,j);
            f1383P3(i,j) = f1383C(i,j);
            f1382P3(i,j) = f1382C(i,j);
            tfrP3(i,j) = tfrC(i,j);
            silP3(i,j) = silC(i,j);
            peakP3(i,j) = peakC(i,j);
            
            for k = 1:4;
                for l = 1:12;
                    sndP3(i,j,l) = sndC(i,j,l);
                    stotP3(i,j,l) = stotC(i,j,l);
                    smP3(i,j,k,l) = smC(i,j,k,l);
                    stP3(i,j,k,l) = stC(i,j,k,l);
                    
                    % Fix wacky snow values
                    % if sndP3(i,j,l) ~= 5e4 && sndP3(i,j,l) >0
                    % sndP3(i,j,l) = 0;
                    % end
                    
                    
                end
            end
            for m = 1:9;
                f1391P3(i,j,m) = f1391C(i,j,m);
            end
            
        end
    end
end


%% Write new Priabonian ice sheets as netCDFs


ncwrite('pri_052.nc', 'landicemaskuk', imP2); % these were not created by libancil
ncwrite('pri_051.nc', 'landicemaskuk', imP3); % these were not created by libancil
ncwrite('pri_052.nc', 'oroguk', topoP2);
ncwrite('pri_051.nc', 'oroguk', topoP3);
ncwrite('pri_052.nc', 'field1395uk', albP2);
ncwrite('pri_051.nc', 'field1395uk', albP3);
ncwrite('pri_052.nc', 'orogsd1auk', sdP2);
ncwrite('pri_051.nc', 'orogsd1auk', sdP3);
ncwrite('pri_052.nc', 'orogxx1auk', xxP2);
ncwrite('pri_051.nc', 'orogxx1auk', xxP3);
ncwrite('pri_052.nc', 'orogxy1auk', xyP2);
ncwrite('pri_051.nc', 'orogxy1auk', xyP3);
ncwrite('pri_052.nc', 'orogyy1auk', yyP2);
ncwrite('pri_051.nc', 'orogyy1auk', yyP3);
ncwrite('pri_052.nc', 'field329uk', f329P2);
ncwrite('pri_051.nc', 'field329uk', f329P3);
ncwrite('pri_052.nc', 'field330uk', f330P2);
ncwrite('pri_051.nc', 'field330uk', f330P3);
ncwrite('pri_052.nc', 'field331uk', f331P2);
ncwrite('pri_051.nc', 'field331uk', f331P3);
ncwrite('pri_052.nc', 'field332uk', f332P2);
ncwrite('pri_051.nc', 'field332uk', f332P3);
ncwrite('pri_052.nc', 'field333uk', f333P2);
ncwrite('pri_051.nc', 'field333uk', f333P3);
ncwrite('pri_052.nc', 'field335uk', f335P2);
ncwrite('pri_051.nc', 'field335uk', f335P3);
ncwrite('pri_052.nc', 'field336uk', f336P2);
ncwrite('pri_051.nc', 'field336uk', f336P3);
ncwrite('pri_052.nc', 'field342uk', f342P2);
ncwrite('pri_051.nc', 'field342uk', f342P3);
ncwrite('pri_052.nc', 'field321uk', f321P2);
ncwrite('pri_051.nc', 'field321uk', f321P3);
ncwrite('pri_052.nc', 'field1381uk', f1381P2);
ncwrite('pri_051.nc', 'field1381uk', f1381P3);
ncwrite('pri_052.nc', 'field1397uk', f1397P2);
ncwrite('pri_051.nc', 'field1397uk', f1397P3);
ncwrite('pri_052.nc', 'field322uk', f322P2);
ncwrite('pri_051.nc', 'field322uk', f322P3);
ncwrite('pri_052.nc', 'field323uk', f323P2);
ncwrite('pri_051.nc', 'field323uk', f323P3);
ncwrite('pri_052.nc', 'field324uk', f324P2);
ncwrite('pri_051.nc', 'field324uk', f324P3);
ncwrite('pri_052.nc', 'field325uk', f325P2);
ncwrite('pri_051.nc', 'field325uk', f325P3);
ncwrite('pri_052.nc', 'field326uk', f326P2);
ncwrite('pri_051.nc', 'field326uk', f326P3);
ncwrite('pri_052.nc', 'field327uk', f327P2);
ncwrite('pri_051.nc', 'field327uk', f327P3);
ncwrite('pri_052.nc', 'field328uk', f328P2);
ncwrite('pri_051.nc', 'field328uk', f328P3);
ncwrite('pri_052.nc', 'snowdepthuk', sndP2);
ncwrite('pri_051.nc', 'snowdepthuk', sndP3);
ncwrite('pri_052.nc', 'soiltotaluk', stotP2);
ncwrite('pri_051.nc', 'soiltotaluk', stotP3);
ncwrite('pri_052.nc', 'soilmoisuk', smP2);
ncwrite('pri_051.nc', 'soilmoisuk', smP3);
ncwrite('pri_052.nc', 'soiltempuk', stP2);
ncwrite('pri_051.nc', 'soiltempuk', stP3);
ncwrite('pri_052.nc', 'field1391uk', f1391P2);
ncwrite('pri_051.nc', 'field1391uk', f1391P3);
ncwrite('pri_052.nc', 'field1384uk', f1384P2);
ncwrite('pri_051.nc', 'field1384uk', f1384P3);
ncwrite('pri_052.nc', 'field1383uk', f1383P2);
ncwrite('pri_051.nc', 'field1383uk', f1383P3);
ncwrite('pri_052.nc', 'field1382uk', f1382P2);
ncwrite('pri_051.nc', 'field1382uk', f1382P3);
ncwrite('pri_052.nc', 'totalfracuk', tfrP2);
ncwrite('pri_051.nc', 'totalfracuk', tfrP3);
ncwrite('pri_052.nc', 'silh1auk', silP2);
ncwrite('pri_051.nc', 'silh1auk', silP3);
ncwrite('pri_052.nc', 'peak1auk', peakP2);
ncwrite('pri_051.nc', 'peak1auk', peakP3);

