% Load winds

%% U component

% Rupelian no ice, glaciated (FAIS) and Chattian ice (EAIS)
u_rupn  = rot90(ncread('../EO_data/tdluna.pdclann.nc', 'u_mm_10m'));
u_rupii = rot90(ncread('../EO_data/tdlupa.pdclann.nc', 'u_mm_10m'));
u_rupi  = rot90(ncread('../EO_data/tdwqea.pdclann.nc', 'u_mm_10m'));

% Chattian no ice, glaciated (EAIS) and Rupelian ice (FAIS)
u_chan  = rot90(ncread('../EO_data/tdluoa.pdclann.nc', 'u_mm_10m'));
u_chai  = rot90(ncread('../EO_data/tdluqa.pdclann.nc', 'u_mm_10m'));
u_chaii = rot90(ncread('../EO_data/tdwqfa.pdclann.nc', 'u_mm_10m'));

% Priabonian no ice, EAIS and FAIS
u_prin  = rot90(ncread('../EO_data/tdwqja.pdclann.nc', 'u_mm_10m'));
u_prii  = rot90(ncread('../EO_data/tdwqma.pdclann.nc', 'u_mm_10m'));
u_priii = rot90(ncread('../EO_data/tdwqla.pdclann.nc', 'u_mm_10m'));

% winds are on a T grid, add a row of zeros at the bottom to make plotting
% easier
u_rupn(73,:)  = 0;
u_rupii(73,:) = 0;
u_chai(73,:)  = 0;
u_chan(73,:)  = 0;
u_rupi(73,:)  = 0;
u_chaii(73,:) = 0;
u_prin(73,:)  = 0;
u_prii(73,:)  = 0;
u_priii(73,:) = 0;


%% V component

% Rupelian no ice, glaciated (FAIS) and Chattian ice (EAIS)
v_rupn  = rot90(ncread('../EO_data/tdluna.pdclann.nc', 'v_mm_10m'));
v_rupii = rot90(ncread('../EO_data/tdlupa.pdclann.nc', 'v_mm_10m'));
v_rupi  = rot90(ncread('../EO_data/tdwqea.pdclann.nc', 'v_mm_10m'));

% Chattian no ice, glaciated (EAIS) and Rupelian ice (FAIS)
v_chan  = rot90(ncread('../EO_data/tdluoa.pdclann.nc', 'v_mm_10m'));
v_chai  = rot90(ncread('../EO_data/tdluqa.pdclann.nc', 'v_mm_10m'));
v_chaii = rot90(ncread('../EO_data/tdwqfa.pdclann.nc', 'v_mm_10m'));

% Priabonian no ice, EAIS and FAIS
v_prin  = rot90(ncread('../EO_data/tdwqja.pdclann.nc', 'v_mm_10m'));
v_prii  = rot90(ncread('../EO_data/tdwqma.pdclann.nc', 'v_mm_10m'));
v_priii = rot90(ncread('../EO_data/tdwqla.pdclann.nc', 'v_mm_10m'));

% winds are on a T grid, add a row of zeros at the bottom to make plotting
% easier
v_rupn(73,:)  = 0;
v_rupii(73,:) = 0;
v_chai(73,:)  = 0;
v_chan(73,:)  = 0;
v_rupi(73,:)  = 0;
v_chaii(73,:) = 0;
v_prin(73,:)  = 0;
v_prii(73,:)  = 0;
v_priii(73,:) = 0;


%% Aggregate together for tidier vector plots

agg = 3; % set aggregartion (3 or 5 are what I have used in past)

[windu_rupn, windv_rupn]   = proc_ave_winds(u_rupn, v_rupn, agg);
[windu_rupi, windv_rupi]   = proc_ave_winds(u_rupii, v_rupii, agg);
[windu_chan, windv_chan]   = proc_ave_winds(u_chan, v_chan, agg);
[windu_chai, windv_chai]   = proc_ave_winds(u_chai, v_chai, agg);
[windu_chaii, windv_chaii] = proc_ave_winds(u_chaii, v_chaii, agg);
[windu_rupii, windv_rupii] = proc_ave_winds(u_rupi, v_rupi, agg);
[windu_prin, windv_prin]   = proc_ave_winds(u_prin, v_prin, agg);
[windu_prii, windv_prii]   = proc_ave_winds(u_prii, v_prii, agg);
% also generate windX and windY arrays for plotting
[windu_priii, windv_priii,windX,windY] = proc_ave_winds(u_priii, v_priii, agg);

