function [out] = time_mean(in)





l = length(in(1,1,1,:));
out = zeros(1,l);

for i = 1:l;
    
    out(i) = nanmean(nanmean(in(:,:,1,i),1),2);
    
end

end