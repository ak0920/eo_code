% plot_mld
% Kennedy et al. 2015, Figure 6


% Load data
load_lsm
load_mld
load color_positive.mat

% Setting fig properties
latlim = [-90 90]; 
lonlim = [0 360];
font = 'Times New Roman';
fontsz = 10;
cax = [0 500];
con_level = 100:50:500; % contour levels

% Select manual or default placing of contours
% con = [];

figure

%% Priabonian no ice

% Southern Ocean
subplot('position', [0.05 0.75 0.6 0.18])
imagesc(mld_prin)
caxis(cax)
colormap(color_positive);

% add coastline and smooth contours
hold on
[c1,h1] = contourf(mld_prin,con_level);
set(h1,'linestyle','none');
plot(lsm_pri_proc(1,:)/3.75+0.5,lsm_pri_proc(2,:)/-2.5+37,'k')

% Add gridlines
plot([1 96], [61 61], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [37 37], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [13 13], ':', 'color', [0.3 0.3 0.3])
plot([17 17], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([33 33], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([49 49], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([65 65], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([81 81], [1 73], ':', 'color', [0.3 0.3 0.3])

% tidy up
axis off
box on
ylim([37 73])
text(-2.3,40, 'a','fontsize',fontsz,'fontname',font)
title('Southern Ocean','fontsize',fontsz,'fontname',font)


% North Atlantic
subplot('position', [0.7 0.75 0.25 0.18])
imagesc(mld_prin)
caxis(cax)
colormap(color_positive);

%  add coastline and smooth contours
hold on
[c1,h1] = contourf(mld_prin,con_level);
set(h1,'linestyle','none');
plot(lsm_pri_proc(1,:)/3.75+0.5,lsm_pri_proc(2,:)/-2.5+37,'k')

% Add gridlines
plot([1 96], [61 61], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [37 37], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [13 13], ':', 'color', [0.3 0.3 0.3])
plot([17 17], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([33 33], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([49 49], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([65 65], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([81 81], [1 73], ':', 'color', [0.3 0.3 0.3])

% tidy up
axis off
box on
title('North Atlantic','fontsize',fontsz,'fontname',font)
ylim([10 25])
xlim([80 96])


%% Priabonian ice

% Southern Ocean
subplot('position', [0.05 0.55 0.6 0.18])
imagesc(mld_priii)
caxis(cax)
colormap(color_positive);

% add cpastline and smooth contours
hold on
[c1,h1] = contourf(mld_priii,con_level);
set(h1,'linestyle','none');
plot(lsm_pri_proc(1,:)/3.75+0.5,lsm_pri_proc(2,:)/-2.5+37,'k')

% Add gridlines
plot([1 96], [61 61], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [37 37], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [13 13], ':', 'color', [0.3 0.3 0.3])
plot([17 17], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([33 33], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([49 49], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([65 65], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([81 81], [1 73], ':', 'color', [0.3 0.3 0.3])

% tidy up
box on
axis off
ylim([37 73])
text(-2.3,40, 'b','fontsize',fontsz,'fontname',font)


% North Atlantic
subplot('position', [0.7 0.55 0.25 0.18])
imagesc(mld_priii)
caxis(cax)
colormap(color_positive);

% add coastline and smooth contours
hold on
[c1,h1] = contourf(mld_priii,con_level);
set(h1,'linestyle','none');
plot(lsm_pri_proc(1,:)/3.75+0.5,lsm_pri_proc(2,:)/-2.5+37,'k')

% Add gridlines
plot([1 96], [61 61], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [37 37], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [13 13], ':', 'color', [0.3 0.3 0.3])
plot([17 17], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([33 33], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([49 49], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([65 65], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([81 81], [1 73], ':', 'color', [0.3 0.3 0.3])

% tidy up
box on
axis off
ylim([10 25])
xlim([80 96])


%% Rupelian no ice

% Southern Ocean
subplot('position', [0.05 0.35 0.6 0.18])
imagesc(mld_rupn)
caxis(cax)
colormap(color_positive);

% add coastline and smooth contours
hold on
[c1,h1] = contourf(mld_rupn,con_level);
set(h1,'linestyle','none');
plot(lsm_rup_proc(1,:)/3.75+0.5,lsm_rup_proc(2,:)/-2.5+37,'k')

% Add gridlines
plot([1 96], [61 61], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [37 37], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [13 13], ':', 'color', [0.3 0.3 0.3])
plot([17 17], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([33 33], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([49 49], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([65 65], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([81 81], [1 73], ':', 'color', [0.3 0.3 0.3])

% tidy up
box on
axis off
ylim([37 73])
text(-2.3,40, 'c','fontsize',fontsz,'fontname',font)


% North Atlantic
subplot('position', [0.7 0.35 0.25 0.18])
imagesc(mld_rupn)
caxis(cax)
colormap(color_positive);

% add coastline and smooth contours
hold on
[c1,h1] = contourf(mld_rupn,con_level);
set(h1,'linestyle','none');
plot(lsm_rup_proc(1,:)/3.75+0.5,lsm_rup_proc(2,:)/-2.5+37,'k')

% Add gridlines
plot([1 96], [61 61], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [37 37], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [13 13], ':', 'color', [0.3 0.3 0.3])
plot([17 17], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([33 33], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([49 49], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([65 65], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([81 81], [1 73], ':', 'color', [0.3 0.3 0.3])

% tidy up
box on
axis off
ylim([10 25])
xlim([80 96])


%% Rupelian ice

% Southern Ocean
subplot('position', [0.05 0.15 0.6 0.18])
imagesc(mld_rupi)
caxis(cax)
colormap(color_positive);

% add coastline and smooth contours
hold on
[c1,h1] = contourf(mld_rupi,con_level);
set(h1,'linestyle','none');
plot(lsm_rup_proc(1,:)/3.75+0.5,lsm_rup_proc(2,:)/-2.5+37,'k')

% Add gridlines
plot([1 96], [61 61], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [37 37], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [13 13], ':', 'color', [0.3 0.3 0.3])
plot([17 17], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([33 33], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([49 49], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([65 65], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([81 81], [1 73], ':', 'color', [0.3 0.3 0.3])

% tiy up
box on
axis off
ylim([37 73])
text(-2.3,40, 'd','fontsize',fontsz,'fontname',font)


% North Atlantic
subplot('position', [0.7 0.15 0.25 0.18])
imagesc(mld_rupi)
caxis(cax)
colormap(color_positive);

% add coastline and smooth contours
hold on
[c1,h1] = contourf(mld_rupi,con_level);
set(h1,'linestyle','none');
plot(lsm_rup_proc(1,:)/3.75+0.5,lsm_rup_proc(2,:)/-2.5+37,'k')

% Add gridlines
plot([1 96], [61 61], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [37 37], ':', 'color', [0.3 0.3 0.3])
plot([1 96], [13 13], ':', 'color', [0.3 0.3 0.3])
plot([17 17], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([33 33], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([49 49], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([65 65], [1 73], ':', 'color', [0.3 0.3 0.3])
plot([81 81], [1 73], ':', 'color', [0.3 0.3 0.3])

% tidy up
box on
axis off
ylim([10 25])
xlim([80 96])


%% Colour bar
color = colorbar('location', 'SouthOutside', 'Position', [0.15 0.1 0.7 0.022]);
xlabel(color, 'Mixed layer depth (m)', 'fontname', font, 'fontsize', fontsz)
set(gca, 'fontname', font, 'fontsize', fontsz)

% final tidy up
subplot('position', [2 1 1 1])
imagesc(sst_rupn)               % truesize doesn't like working with contour plots
truesize([510 510])
set(gcf, 'color', 'w');

