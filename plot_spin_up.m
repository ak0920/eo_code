%% Add spin up time series

load_spinup;
font = 'Times New Roman';
fontsz = 10;
lw = 2; 

figure;

%% Plot first part of time series (if appropriate)
plot (timepn(1:369), tspn(1:369), '-','color', [0.0585574759170413,0.364924505352974,0.679114878177643], 'linewidth', lw)
hold on
plot (timepn(1:369), tspi(1:369), '--','color', [0.0585574759170413,0.364924505352974,0.679114878177643], 'linewidth', lw)
plot (timern(1:358), tsrn(1:358), '-','color',[0.850000000000000,0.0872549042105675,0], 'linewidth', lw)
plot (timeri(1:358), tsri(1:358), '--','color',[0.850000000000000,0.0872549042105675,0], 'linewidth', lw)

xlabel('Spin-up time (years)', 'fontname', font, 'fontsize', fontsz)
ylabel('Ocean temperature at 670 m (�C)', 'fontname', font, 'fontsize', fontsz)
set(gca, 'fontname', font, 'fontsize', fontsz)
legend('Priabonian no AIS', 'Priabonian AIS','Rupelian no AIS', 'Rupelian AIS', 'location','NorthEast')

%% Plot second part of time series (if appropriate)
plot (timepn(500:1552)-50, tspn(500:1552), '-','color', [0.0585574759170413,0.364924505352974,0.679114878177643], 'linewidth', lw)
plot (timepn(500:1552)-50, tspi(500:1552), '--','color', [0.0585574759170413,0.364924505352974,0.679114878177643], 'linewidth', lw)
plot (timern(405:1458)-50, tsrn(405:1458), '-','color',[0.850000000000000,0.0872549042105675,0], 'linewidth', lw)
plot (timeri(405:1458)-50, tsri(405:1458), '--','color',[0.850000000000000,0.0872549042105675,0], 'linewidth', lw)

ylim([7 12])

text(-350,11.5, 'c','fontsize',fontsz,'fontname',font)
text(90,7.3, 'Phase 2','fontsize',fontsz,'fontname',font)
text(500,7.3, 'Phase 4','fontsize',fontsz,'fontname',font)


% Add grey boxes at bottom for each phase
p1 = fill([0 50 50 0],[7 7 7.15 7.15],[0.8 0.8 0.8]);
p2 = fill([50 369 369 50],[7 7 7.15 7.15],[0.9 0.9 0.9]);
p3 = fill([369 422 422 369],[7 7 7.15 7.15],[0.8 0.8 0.8]);
p4 = fill([422 1422 1422 422],[7 7 7.15 7.15],[0.9 0.9 0.9]);

