%% Plot the SST in glorious HD

% SST, sea ice, P, wind and surface current super plot

%% Load data

    load_sst_sat_vars


load scale_col3.mat

%% Land sea masks

lsm_rup_proc = lsm_proc(flipud(isnan(sst_rupn)*1)); % lsm_proc(lsm_rup);
lsm_cha_proc = lsm_proc(flipud(isnan(sst_chan)*1)); % lsm_proc(lsm_cha);
lsm_pri_proc = lsm_proc(flipud(isnan(sst_prin)*1)); % lsm_proc(lsm_cha);
close(gcf)

%% SAT Plots

maps = figure;

% Priabonian
subplot('position', [0.1 0.755 0.7 0.24])

sst_pridw = sst_priiw-sst_prinw;
sst_int_pridw = sst_int_priiw-sst_int_prinw;
sst_pridw(sst_pridw<-6) = -6;
sst_int_pridw(sst_int_pridw<-6) = -6;

axesm('MapProjection','Robinson', 'MapLatLim', latlim,... % eqdcylin    Robinson
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,sst_pridw) % Coarse grid up to LSM
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])
cax = [-6.25 6.25];
caxis(cax)
colormap(scale_col3);

hold on
% Add smooth grid
pcolorm(lat_lsm,lon_lsm,flipud(sst_int_pridw));
% sst_rup_g = 1 * isnan(sst_rupi);
% pcolorm(lat_lsm,lon_lsm,sst_rup_g)
% geoshow(lsm_con_low_rup1(1,1:980), lsm_con_low_rup1(2,1:980), 'DisplayType','polygon','FaceColor',[0.85 0.85 0.85])
plotm(lsm_pri_proc(2,:), lsm_pri_proc(1,:), 'k-'); % LSM
% cr = contourm(lat_lsm,lon_lsm,(ice_int_prii-ice_int_prin),[-1:0.05:-0.2],'-','color', [0.850000000000000,0.0872549042105675,0]);
% contourm(lat_lsm,lon_lsm,(ice_int_prii-ice_int_prin),[0.2:0.05:1],'-','color',[0.0585574759170413,0.364924505352974,0.679114878177643]);
% contourm(lat_lsm,lon_lsm,(ice_int_prii-ice_int_prin),[0],'k-');

tightmap
box off
axis off



% SAT Rup
subplot('position', [0.1 0.505 0.7 0.24])
sst_prids = sst_priis-sst_prins;
sst_int_prids = sst_int_priis-sst_int_prins;
sst_prids(sst_prids<-6) = -6;
sst_int_prids(sst_int_prids<-6) = -6;

axesm('MapProjection','Robinson', 'MapLatLim', latlim,... % eqdcylin    Robinson
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,sst_prids) % Coarse grid up to LSM
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])
cax = [-6.25 6.25];
caxis(cax)
colormap(scale_col3);

hold on
% Add smooth grid
pcolorm(lat_lsm,lon_lsm,flipud(sst_int_prids));
% sst_rup_g = 1 * isnan(sst_rupi);
% pcolorm(lat_lsm,lon_lsm,sst_rup_g)
% geoshow(lsm_con_low_rup1(1,1:980), lsm_con_low_rup1(2,1:980), 'DisplayType','polygon','FaceColor',[0.85 0.85 0.85])
plotm(lsm_pri_proc(2,:), lsm_pri_proc(1,:), 'k-'); % LSM
% cr = contourm(lat_lsm,lon_lsm,(ice_int_rupi-ice_int_rupn),[-1:0.2:-0.2],'k-');
% contourm(lat_lsm,lon_lsm,(ice_int_rupi-ice_int_rupn),[0.2:0.2:1],'k--');

text(-2.3,4.1, 'a','fontsize',fontsz,'fontname',font)
text(-2.3,1.275, 'b','fontsize',fontsz,'fontname',font)
tightmap
box off
axis off



% % Add colorbar
% subplot('position', [20 0 1 1])
% 
% caxis(cax)
% colormap(scale_col3);
% color = colorbar('location', 'EastOutside', 'Position', [0.8 0.575 0.022 0.35]);
% set(color, 'ytick', [-30:5:30], 'ylim', [-30 30])
% ylabel(color, 'Surface Air Temperature difference (�C)', 'fontname', font, 'fontsize', fontsz)
% set(gca, 'fontname', font, 'fontsize', fontsz)
% truesize([680 510])
% 
% box off
% axis off
% 
% freezeColors
% cblabel(colorbar);
% cbunits(colorbar);
% cbfreeze(colorbar);



% SST Pri
subplot('position',[0.1 0.255 0.7 0.24])

sst_rupdw = sst_rupiw-sst_rupnw;
sst_int_rupdw = sst_int_rupiw-sst_int_rupnw;
sst_rupdw(sst_rupdw<-6) = -6;
sst_int_rupdw(sst_int_rupdw<-6) = -6;

axesm('MapProjection','Robinson', 'MapLatLim', latlim,... % eqdcylin    Robinson
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,sst_rupdw) % Coarse grid up to LSM
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])
cax = [-6.25 6.25];
caxis(cax)
h1 = colormap(scale_col3);

hold on
% Add smooth grid
h2 = pcolorm(lat_lsm,lon_lsm,flipud(sst_int_rupdw));
% sst_rup_g = 1 * isnan(sst_rupi);
% pcolorm(lat_lsm,lon_lsm,sst_rup_g)
% geoshow(lsm_con_low_rup1(1,1:980), lsm_con_low_rup1(2,1:980), 'DisplayType','polygon','FaceColor',[0.85 0.85 0.85])
h3 = plotm(lsm_rup_proc(2,:), lsm_rup_proc(1,:), 'k-'); % LSM

tightmap
box off
axis off


%% SST Plots

% Rupelian
subplot('position', [0.1 0.005 0.7 0.24])

sst_rupds = sst_rupis-sst_rupns;
sst_int_rupds = sst_int_rupis-sst_int_rupns;
sst_rupds(sst_rupds<-6) = -6;
sst_int_rupds(sst_int_rupds<-6) = -6;

axesm('MapProjection','Robinson', 'MapLatLim', latlim,... % eqdcylin    Robinson
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,sst_rupds) % Coarse grid up to LSM
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])
caxis(cax)
h1 = colormap(scale_col3);

hold on
% Add smooth grid
h2 = pcolorm(lat_lsm,lon_lsm,flipud(sst_int_rupds));
% sst_rup_g = 1 * isnan(sst_rupi);
% pcolorm(lat_lsm,lon_lsm,sst_rup_g)
% geoshow(lsm_con_low_rup1(1,1:980), lsm_con_low_rup1(2,1:980), 'DisplayType','polygon','FaceColor',[0.85 0.85 0.85])
h3 = plotm(lsm_rup_proc(2,:), lsm_rup_proc(1,:), 'k-'); % LSM

text(-2.3,4.1, 'c','fontsize',fontsz,'fontname',font)
text(-2.3,1.275, 'd','fontsize',fontsz,'fontname',font)
tightmap
box off
axis off

% % Chattian
% subplot('position', [0.1 0.005 0.7 0.24])
% 
% sst_chad = sst_chai-sst_chan;
% sst_int_chad = sst_int_chai-sst_int_chan;
% sst_chad(sst_chad<-6) = -6;
% sst_int_chad(sst_int_chad<-6) = -6;
% 
% axesm('MapProjection','Robinson', 'MapLatLim', latlim,... % eqdcylin    Robinson
%     'MapLonLim', lonlim, 'MLineLocation', 60,...
%     'PlineLocation', 60, 'MLabelParallel', 'south')
% pcolorm(lat_lsm,lon_lsm,sst_chad) % Coarse grid up to LSM
% framem('FEdgeColor', 'black', 'FLineWidth', 1)
% gridm('Gcolor',[0.3 0.3 0.3])
% caxis(cax)
% colormap(scale_col3);
% hold on
% % Add smooth grid
% pcolorm(lat_lsm,lon_lsm,flipud(sst_int_chad));
% % sst_rup_g = 1 * isnan(sst_rupi);
% % pcolorm(lat_lsm,lon_lsm,sst_rup_g)
% plotm(lsm_cha_proc(2,:), lsm_cha_proc(1,:), 'k-'); % LSM
% 
% text(-2.3,4.1, 'c','fontsize',fontsz,'fontname',font)
% text(-2.3,1.275, 'd','fontsize',fontsz,'fontname',font)
% % text(-2.875,1.125, 'ice - no ice','fontsize',fontsz,'fontname',font)
% % text(-8.875,1.125, 'ice - no ice','fontsize',fontsz,'fontname',font)
% tightmap
% box off
% axis off

% Add colorbar
subplot('position', [2 0 1 1])
caxis(cax)
colormap(scale_col3);
color = colorbar('location', 'EastOutside', 'Position', [0.8 0.25 0.022 0.5]);
set(color, 'ytick', [-6:1:6], 'ylim',[-6 6])
ylabel(color, 'Sea Surface Temperature difference (�C)', 'fontname', font, 'fontsize', fontsz)
set(gca, 'fontname', font, 'fontsize', fontsz)
truesize([680 510])

box off
axis off

freezeColors
cblabel(colorbar);
cbunits(colorbar);
cbfreeze(colorbar);

set(gcf, 'color', 'w');

