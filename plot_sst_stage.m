%% Plot the SST in glorious HD

% SST, sea ice, P, wind and surface current super plot

%% Load data

    load_sst_sat_vars


load scale_col3.mat

%% Land sea masks

lsm_rup_proc = lsm_proc(flipud(isnan(sst_rupn)*1)); % lsm_proc(lsm_rup);
lsm_cha_proc = lsm_proc(flipud(isnan(sst_chan)*1)); % lsm_proc(lsm_cha);
lsm_pri_proc = lsm_proc(flipud(isnan(sst_prin)*1)); % lsm_proc(lsm_cha);
close(gcf)

%% SAT Plots

maps = figure;



% SST Pri
subplot('position',[0.1 0.55 0.7 0.35])

sst_prid = sst_rupn-sst_prin;
sst_int_prid = sst_int_rupn-sst_int_prin;
sst_prid(sst_prid<-6) = -6;
sst_int_prid(sst_int_prid<-6) = -6;

axesm('MapProjection','Robinson', 'MapLatLim', latlim,... % eqdcylin    Robinson
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,sst_prid) % Coarse grid up to LSM
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])
cax = [-6.25 6.25];
caxis(cax)
h1 = colormap(scale_col3);

hold on
% Add smooth grid
h2 = pcolorm(lat_lsm,lon_lsm,flipud(sst_int_prid));
% sst_rup_g = 1 * isnan(sst_rupi);
% pcolorm(lat_lsm,lon_lsm,sst_rup_g)
% geoshow(lsm_con_low_rup1(1,1:980), lsm_con_low_rup1(2,1:980), 'DisplayType','polygon','FaceColor',[0.85 0.85 0.85])
h3 = plotm(lsm_pri_proc(2,:), lsm_pri_proc(1,:), 'k-'); % LSM
text(-2.3,1.275, 'a','fontsize',fontsz,'fontname',font)

tightmap
box off
axis off


%% SST Plots

% Rupelian
subplot('position', [0.1 0.1 0.7 0.35])

sst_rupd = sst_rupi-sst_priii;
sst_int_rupd = sst_int_rupi-sst_int_priii;
sst_rupd(sst_rupd<-6) = -6;
sst_int_rupd(sst_int_rupd<-6) = -6;

axesm('MapProjection','Robinson', 'MapLatLim', latlim,... % eqdcylin    Robinson
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,sst_rupd) % Coarse grid up to LSM
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])
caxis(cax)
h1 = colormap(scale_col3);

hold on
% Add smooth grid
h2 = pcolorm(lat_lsm,lon_lsm,flipud(sst_int_rupd));
% sst_rup_g = 1 * isnan(sst_rupi);
% pcolorm(lat_lsm,lon_lsm,sst_rup_g)
% geoshow(lsm_con_low_rup1(1,1:980), lsm_con_low_rup1(2,1:980), 'DisplayType','polygon','FaceColor',[0.85 0.85 0.85])
h3 = plotm(lsm_pri_proc(2,:), lsm_pri_proc(1,:), 'k-'); % LSM

text(-2.3,1.275, 'b','fontsize',fontsz,'fontname',font)
tightmap
box off
axis off

% % Chattian
% subplot('position', [0.1 0.005 0.7 0.24])
% 
% sst_chad = sst_chai-sst_chan;
% sst_int_chad = sst_int_chai-sst_int_chan;
% sst_chad(sst_chad<-6) = -6;
% sst_int_chad(sst_int_chad<-6) = -6;
% 
% axesm('MapProjection','Robinson', 'MapLatLim', latlim,... % eqdcylin    Robinson
%     'MapLonLim', lonlim, 'MLineLocation', 60,...
%     'PlineLocation', 60, 'MLabelParallel', 'south')
% pcolorm(lat_lsm,lon_lsm,sst_chad) % Coarse grid up to LSM
% framem('FEdgeColor', 'black', 'FLineWidth', 1)
% gridm('Gcolor',[0.3 0.3 0.3])
% caxis(cax)
% colormap(scale_col3);
% hold on
% % Add smooth grid
% pcolorm(lat_lsm,lon_lsm,flipud(sst_int_chad));
% % sst_rup_g = 1 * isnan(sst_rupi);
% % pcolorm(lat_lsm,lon_lsm,sst_rup_g)
% plotm(lsm_cha_proc(2,:), lsm_cha_proc(1,:), 'k-'); % LSM
% 
% text(-2.3,4.1, 'c','fontsize',fontsz,'fontname',font)
% text(-2.3,1.275, 'd','fontsize',fontsz,'fontname',font)
% % text(-2.875,1.125, 'ice - no ice','fontsize',fontsz,'fontname',font)
% % text(-8.875,1.125, 'ice - no ice','fontsize',fontsz,'fontname',font)
% tightmap
% box off
% axis off

% Add colorbar
subplot('position', [2 0 1 1])
caxis(cax)
colormap(scale_col3);
color = colorbar('location', 'EastOutside', 'Position', [0.85 0.25 0.022 0.5]);
set(color, 'ytick', [-6:1:6], 'ylim',[-6 6])
ylabel(color, 'Sea Surface Temperature difference (�C)', 'fontname', font, 'fontsize', fontsz)
set(gca, 'fontname', font, 'fontsize', fontsz)
truesize([480 510])

box off
axis off

freezeColors
cblabel(colorbar);
cbunits(colorbar);
cbfreeze(colorbar);

set(gcf, 'color', 'w');

