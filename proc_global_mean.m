function [out] = proc_global_mean(in)
% [out] = time_mean(in)
% 
% This calculates the global mean from the temperature time series


l = length(in(1,1,1,:));
out = zeros(1,l);

for i = 1:l;
    
    out(i) = nanmean(nanmean(in(:,:,1,i),1),2);
    
end

end