%% Plot the SST in glorious HD

% SST, sea ice, P, wind and surface current super plot

%% Load data

    load_salinity_vars
    load_w_p_vars
    load_sst_p_vars


%% Land sea masks

lsm_rup_proc = lsm_proc(flipud(isnan(sst_rupn)*1)); % lsm_proc(lsm_rup);
lsm_pri_proc = lsm_proc(flipud(isnan(sst_prin)*1)); % lsm_proc(lsm_cha);
close(gcf)

windX1 = (windX)*2;
windY1 = (windY-91)*-2;
v_sc = 2; % vector scale

salran = [33 40];



%% Plot Pressure and Winds - Chattian
subplot('position', [0.05 0.255 0.8 0.24])
% subplot(2,1,2)


if ~exist('salh_rupn') 
    salh_rupn = inc_low_res_pixels2(sal_rupn);
    salh_rupi = inc_low_res_pixels2(sal_rupi);
end 

sal_int_rupn2 = expand_int_seas(sal_int_rupn,sal_rupn);
sal_int_rupi2 = expand_int_seas(sal_int_rupi,sal_rupi);
sal_int_rupd = sal_int_rupi2 - sal_int_rupn2;

sal_int_rupd(sal_int_rupd<-0.005) = -0.005;



imagesc(sal_int_rupn2);

cax = salran;
caxis(cax)
colormap(color_positive);

hold on
% plot(lsmorx,lsmory,'k')

ylim([120 360])
xlim([1 713])

% imagesc(jja_int_rupi(:,1:713)-jja_int_rupn(:,1:713))
plot(lsm_rup_proc(1,:)*2, (lsm_rup_proc(2,:)-90)*-2,'k-')
% 
plot([1 720], [181 181], ':', 'color', [0.3 0.3 0.3])
plot([1 720], [301 301], ':', 'color', [0.3 0.3 0.3])
plot([121 121], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([241 241], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([361 361], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([481 481], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([601 601], [1 360], ':', 'color', [0.3 0.3 0.3])


set(gca, 'xticklabel', '', 'yticklabel','', 'xtick', [], 'ytick', [])


q1 = quiver(windX1, windY1,curu_rupn, (curv_rupn)*-1, 0,'k');
qU = get(q1,'UData');
qV = get(q1,'VData');
set(q1,'UData',v_sc*qU, 'VData',v_sc*qV);

% % scale bar
% fill([10 150 150 10],[130 130 174 174],'w');
% 
% plot([22 46], [152 152], 'k-', 'linewidth',2);

box on


text(-20,150, 'c','fontsize',fontsz,'fontname',font)
% t1 = text(-20,300, 'Rupelian DJF','fontsize',fontsz,'fontname',font);
% set(t1,'rotation',90)

%% RUPI
subplot('position', [0.05 0.005 0.8 0.24])
% subplot(2,1,2)


if ~exist('salh_rupn') 
    salh_rupn = inc_low_res_pixels2(sal_rupn);
    salh_rupi = inc_low_res_pixels2(sal_rupi);
end 

sal_int_rupn2 = expand_int_seas(sal_int_rupn,sal_rupn);
sal_int_rupi2 = expand_int_seas(sal_int_rupi,sal_rupi);
sal_int_rupd = sal_int_rupi2 - sal_int_rupn2;

sal_int_rupd(sal_int_rupd<-0.005) = -0.005;



imagesc(sal_int_rupi2);

cax = salran;
caxis(cax)
colormap(color_positive);

hold on
% plot(lsmorx,lsmory,'k')

ylim([120 360])
xlim([1 713])

% imagesc(jja_int_rupi(:,1:713)-jja_int_rupn(:,1:713))
plot(lsm_rup_proc(1,:)*2, (lsm_rup_proc(2,:)-90)*-2,'k-')
% 
plot([1 720], [181 181], ':', 'color', [0.3 0.3 0.3])
plot([1 720], [301 301], ':', 'color', [0.3 0.3 0.3])
plot([121 121], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([241 241], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([361 361], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([481 481], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([601 601], [1 360], ':', 'color', [0.3 0.3 0.3])


set(gca, 'xticklabel', '', 'yticklabel','', 'xtick', [], 'ytick', [])


q1 = quiver(windX1, windY1,curu_rupi, (curv_rupi)*-1, 0,'k');
qU = get(q1,'UData');
qV = get(q1,'VData');
set(q1,'UData',v_sc*qU, 'VData',v_sc*qV);

% % scale bar
% fill([10 150 150 10],[130 130 174 174],'w');
% 
% plot([22 46], [152 152], 'k-', 'linewidth',2);

box on


text(-20,150, 'd','fontsize',fontsz,'fontname',font)
% t1 = text(-20,300, 'Rupelian DJF','fontsize',fontsz,'fontname',font);
% set(t1,'rotation',90)


%% Upwelling and currents - Chattian
s = subplot('position', [0.05 0.755 0.8 0.24]);

sal_int_prin2 = expand_int_seas(sal_int_prin,sal_prin);
sal_int_prii2 = expand_int_seas(sal_int_prii,sal_prii);

imagesc(sal_int_prin2);

caxis(cax)
colormap(color_positive);

hold on
% plot(lsmorx,lsmory,'k')

ylim([120 360])
xlim([1 713])

% imagesc(jja_int_rupi(:,1:713)-jja_int_rupn(:,1:713))
plot(lsm_pri_proc(1,:)*2, (lsm_pri_proc(2,:)-90)*-2,'k-')
% 
plot([1 720], [181 181], ':', 'color', [0.3 0.3 0.3])
plot([1 720], [301 301], ':', 'color', [0.3 0.3 0.3])
plot([121 121], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([241 241], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([361 361], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([481 481], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([601 601], [1 360], ':', 'color', [0.3 0.3 0.3])


set(gca, 'xticklabel', '', 'yticklabel','', 'xtick', [], 'ytick', [])

q1 = quiver(windX1, windY1,curu_prin, (curv_prin)*-1, 0,'k');
qU = get(q1,'UData');
qV = get(q1,'VData');
set(q1,'UData',v_sc*qU, 'VData',v_sc*qV);

% scale bar
fill([10 150 150 10],[130 130 174 174],'w');
t1 = text(60,148, '25 cm s^-^1','fontsize',fontsz,'fontname',font);
plot([22 46], [152 152], 'k-', 'linewidth',2);
box on


text(-20,150, 'a','fontsize',fontsz,'fontname',font)
% t1 = text(-20,300, 'Chattian DJF','fontsize',fontsz,'fontname',font);
% set(t1,'rotation',90)

%% Upwelling and currents - Chattian
s = subplot('position', [0.05 0.505 0.8 0.24]);

imagesc(sal_int_prii2);

caxis(cax)
colormap(color_positive);

hold on
% plot(lsmorx,lsmory,'k')

ylim([120 360])
xlim([1 713])

% imagesc(jja_int_rupi(:,1:713)-jja_int_rupn(:,1:713))
plot(lsm_pri_proc(1,:)*2, (lsm_pri_proc(2,:)-90)*-2,'k-')
% 
plot([1 720], [181 181], ':', 'color', [0.3 0.3 0.3])
plot([1 720], [301 301], ':', 'color', [0.3 0.3 0.3])
plot([121 121], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([241 241], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([361 361], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([481 481], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([601 601], [1 360], ':', 'color', [0.3 0.3 0.3])


set(gca, 'xticklabel', '', 'yticklabel','', 'xtick', [], 'ytick', [])

box on


q1 = quiver(windX1, windY1,curu_prii, (curv_prii)*-1, 0,'k');
qU = get(q1,'UData');
qV = get(q1,'VData');
set(q1,'UData',v_sc*qU, 'VData',v_sc*qV);

% % scale bar
% fill([10 150 150 10],[130 130 174 174],'w');
% 
% plot([22 46], [152 152], 'k-', 'linewidth',2);
text(-20,150, 'b','fontsize',fontsz,'fontname',font)
% t1 = text(-20,300, 'Chattian DJF','fontsize',fontsz,'fontname',font);
% set(t1,'rotation',90)


% Add colorbar
subplot('position', [2 0 1 1])

caxis(cax)
colormap(color_positive);
color = colorbar('location', 'EastOutside', 'Position', [0.87 0.2 0.022 0.6]);
% DIRTY MANUALLY SET LAND TO GREY
ylim(color,[33 40])
set(color, 'ytick', [33:0.5:40])
ylabel(color, 'Surface salinity (PSU)', 'fontname', font, 'fontsize', fontsz)
set(gca, 'fontname', font, 'fontsize', fontsz)
truesize([680 510])



truesize([680 510])

set(gcf, 'color', 'w');


