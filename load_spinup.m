% load_spinup


%% Load temperature time series

% Phase 1 and 2 records
r12 = ncread('../EO_data/tdkah.oceantemppg12.monthly.nc','temp_ym_dpth');
c12 = ncread('../EO_data/tdkai.oceantemppg12.monthly.nc','temp_ym_dpth');
p12 = ncread('../EO_data/tdkag.oceantemppg12.monthly.nc','temp_ym_dpth');

% Phase 3 records
% This phase has seperate ice/no ice for each stage
rn3 = ncread('../EO_data/tdlch.oceantemppg12.monthly.nc','temp');
ri3 = ncread('../EO_data/tdlcl.oceantemppg12.monthly.nc','temp');
cn3 = ncread('../EO_data/tdlci.oceantemppg12.monthly.nc','temp');
ci3 = ncread('../EO_data/tdlcm.oceantemppg12.monthly.nc','temp');
pn3 = ncread('../EO_data/tdlcg.oceantemppg12.monthly.nc','temp');
pi3 = ncread('../EO_data/tdlck.oceantemppg12.monthly.nc','temp');

% Phase 4 records 
% note some are in 2 parts and there are ice/no ice for each stage
rn4a = ncread('../EO_data/tdlun.oceantemppg12.monthly.nc','temp_ym_dpth');
rn4b = ncread('../EO_data/tdlut.oceantemppg12.monthly.nc','temp_ym_dpth');
ri4  = ncread('../EO_data/tdlup.oceantemppg12.monthly.nc','temp_ym_dpth');
cn4a = ncread('../EO_data/tdluo.oceantemppg12.monthly.nc','temp_ym_dpth');
cn4b = ncread('../EO_data/tdluu.oceantemppg12.monthly.nc','temp_ym_dpth');
ci4  = ncread('../EO_data/tdluq.oceantemppg12.monthly.nc','temp_ym_dpth');
pn4  = ncread('../EO_data/tdwqj.oceantemppg12.monthly.nc','temp');
pi4  = ncread('../EO_data/tdwqm.oceantemppg12.monthly.nc','temp');
pii4 = ncread('../EO_data/tdwql.oceantemppg12.monthly.nc','temp');


%% Load time for each phase

% Phase 1 and 2
r12t = ncread('../EO_data/tdkah.oceantemppg12.monthly.nc','t'); % 'days since 1850-12-01 00:00:00'
c12t = ncread('../EO_data/tdkai.oceantemppg12.monthly.nc','t'); % 'days since 1850-12-01 00:00:00'
p12t = ncread('../EO_data/tdkag.oceantemppg12.monthly.nc','t'); % 'days since 1850-12-01 00:00:00'

% Phase 3
rn3t  = ncread('../EO_data/tdlch.oceantemppg12.monthly.nc','time'); % 'days since 2219-12-01 00:00:00'
ri3t  = ncread('../EO_data/tdlcl.oceantemppg12.monthly.nc','time'); % 'days since 2219-12-01 00:00:00'
cn3t  = ncread('../EO_data/tdlci.oceantemppg12.monthly.nc','time'); % 'days since 2219-12-01 00:00:00'
ci3t  = ncread('../EO_data/tdlcm.oceantemppg12.monthly.nc','time'); % 'days since 2219-12-01 00:00:00'
pn3t  = ncread('../EO_data/tdlcg.oceantemppg12.monthly.nc','time'); % 'days since 2219-12-01 00:00:00'
pii3t = ncread('../EO_data/tdlck.oceantemppg12.monthly.nc','time'); % 'days since 2219-12-01 00:00:00'

% Phase 4
rn4at = ncread('../EO_data/tdlun.oceantemppg12.monthly.nc','t'); % 'days since 2274-12-01 00:00:00'
rn4bt = ncread('../EO_data/tdlut.oceantemppg12.monthly.nc','t'); % 'days since 2859-12-01 00:00:00'
ri4t  = ncread('../EO_data/tdlup.oceantemppg12.monthly.nc','t'); % 'days since 2274-12-01 00:00:00'
cn4at = ncread('../EO_data/tdluo.oceantemppg12.monthly.nc','t'); % 'days since 2274-12-01 00:00:00'
cn4bt = ncread('../EO_data/tdluu.oceantemppg12.monthly.nc','t'); % 'days since 2786-12-01 00:00:00'
ci4t  = ncread('../EO_data/tdluq.oceantemppg12.monthly.nc','t');
pn4t  = ncread('../EO_data/tdwqj.oceantemppg12.monthly.nc','t'); % 'days since 2274-12-01 00:00:00'
pi4t  = ncread('../EO_data/tdwqm.oceantemppg12.monthly.nc','t'); % 'days since 2786-12-01 00:00:00'
pii4t = ncread('../EO_data/tdwql.oceantemppg12.monthly.nc','t');


%% Make combined time for whole spin-up in years

% Rupelian P1 is missing first 11 years
% P2 restarts time stamp, requiring 50 to be added to all values
% P2 is also a different length for each stage
%  - this results in different length records which I only correct when
%    plotting by missingo ut the parts of the records that overrun P2
% Time becomes consistent at P3 and P4
% Further complications arise in P4 in the RupNoIce and ChaNoIce runs which
%    crashed halfway through and needed restarted.
timern  = [r12t(1:39)/360; ((r12t(40:406)/360) + 50); ((rn3t/360) + 2219-1800); ((rn4at/360) + 2274-1800); ((rn4bt/360) + 2859-1800)];

timeri  = [r12t(1:39)/360; ((r12t(40:406)/360) + 50); ((ri3t/360) + 2219-1800); ((ri4t/360) + 2274-1800)];

timecn  = [c12t(1:50)/360; ((c12t(51:403)/360) + 50); ((cn3t/360) + 2219-1800); ((cn4at(1:511)/360) + 2274-1800); ((cn4bt/360) + 2786-1800)]; % 2786

timecn2 = [timecn(timecn<986);timecn(timecn>1051)];

timeci  = [c12t(1:50)/360; ((c12t(51:403)/360) + 50); ((ci3t/360) + 2219-1800); ((ci4t/360) + 2274-1800)];

timepn  = [p12t(1:50)/360; ((p12t(51:499)/360) + 50); ((pn3t/360) + 2219-1800); ((pn4t/360) + 2274-1800)]; 

timepii = [p12t(1:50)/360; ((p12t(51:499)/360) + 50) ;((pii3t/360) + 2219-1800); ((pii4t/360) + 2274-1800)];

odd_time = proc_global_mean(cn4a);

%% Make temperature time series for whole spin-up
tsrn = [proc_global_mean(r12),proc_global_mean(rn3),proc_global_mean(rn4a),proc_global_mean(rn4b)];
tsri = [proc_global_mean(r12),proc_global_mean(ri3),proc_global_mean(ri4)];
tscn = [proc_global_mean(c12),proc_global_mean(cn3),odd_time(1:511),proc_global_mean(cn4b)];
tscn2 = [tscn(timecn<986),tscn(timecn>1051)];
tsci = [proc_global_mean(c12),proc_global_mean(ci3),proc_global_mean(ci4)];
tspn = [proc_global_mean(p12),proc_global_mean(pn3),proc_global_mean(pn4)];
tspi = [proc_global_mean(p12),proc_global_mean(pi3),proc_global_mean(pii4)];


