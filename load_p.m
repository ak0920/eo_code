% load_p

load_latlon % necessary for meshing and interpolating later


%% Load hPas

% Rupelian no ice, glaciated (FAIS) and Chattian ice (EAIS)
P_rupn  = ncread('../EO_data/tdluna.pcclann.nc', 'ht_mm_p');
P_rupii = ncread('../EO_data/tdlupa.pcclann.nc', 'ht_mm_p');
P_rupi  = ncread('../EO_data/tdwqea.pcclann.nc', 'ht_mm_p');

% Chattian no ice, glaciated (EAIS) and Rupelian ice (FAIS)
P_chan = ncread('../EO_data/tdluoa.pcclann.nc', 'ht_mm_p');
P_chai = ncread('../EO_data/tdluqa.pcclann.nc', 'ht_mm_p');
P_chaii = ncread('../EO_data/tdwqfa.pcclann.nc', 'ht_mm_p');

% Priabonian no ice, EAIS and FAIS
P_prin = ncread('../EO_data/tdwqja.pcclann.nc', 'ht_mm_p');
P_prii = ncread('../EO_data/tdwqma.pcclann.nc', 'ht_mm_p');
P_priii = ncread('../EO_data/tdwqla.pcclann.nc', 'ht_mm_p');


%% Select 500 hPa level

% Rupelian no ice, glaciated (FAIS) and Chattian ice (EAIS)
p850_rupn = flipud(rot90(P_rupn(:,:,6)));
p850_rupi = flipud(rot90(P_rupi(:,:,6)));
p850_rupii = flipud(rot90(P_rupii(:,:,6)));

% Chattian no ice, glaciated (EAIS) and Rupelian ice (FAIS)
p850_chan = flipud(rot90(P_chan(:,:,6)));
p850_chai = flipud(rot90(P_chai(:,:,6)));
p850_chaii = flipud(rot90(P_chaii(:,:,6)));

% Priabonian no ice, EAIS and FAIS
p850_prin = flipud(rot90(P_prin(:,:,6)));
p850_prii = flipud(rot90(P_prii(:,:,6)));
p850_priii = flipud(rot90(P_priii(:,:,6)));


%% Interpolate P to high res
% in hindsight this could be replaced with contour plots

% generate grids to mesh between
[low_resX, low_resY] = meshgrid(lon,lat);
[hi_resX, hi_resY]   = meshgrid(lon_lsm, lat_lsm);

% as above
P_int_rupn = interp2(low_resX,low_resY,p850_rupn,hi_resX,hi_resY);
P_int_rupii = interp2(low_resX,low_resY,p850_rupii,hi_resX,hi_resY);
P_int_rupi = interp2(low_resX,low_resY,p850_rupi,hi_resX,hi_resY);

P_int_chan = interp2(low_resX,low_resY,p850_chan,hi_resX,hi_resY);
P_int_chai = interp2(low_resX,low_resY,p850_chai,hi_resX,hi_resY);
P_int_chaii = interp2(low_resX,low_resY,p850_chaii,hi_resX,hi_resY);

P_int_prin = interp2(low_resX,low_resY,p850_prin,hi_resX,hi_resY);
P_int_prii = interp2(low_resX,low_resY,p850_prii,hi_resX,hi_resY);
P_int_priii = interp2(low_resX,low_resY,p850_priii,hi_resX,hi_resY);

