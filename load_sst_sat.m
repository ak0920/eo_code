% load_sst_sat_vars

load_latlon % necessary for meshing and interpolating later


%% Load SSTs annual

% Rupelian no ice, glaciated (FAIS) and Chattian ice (EAIS)
sst_rupn  = rot90(ncread('../EO_data/tdluto.pfclann.nc', 'temp_mm_uo'));
sst_rupii = rot90(ncread('../EO_data/tdlupo.pfclann.nc', 'temp_mm_uo'));
sst_rupi  = rot90(ncread('../EO_data/tdwqeo.pfclann.nc', 'temp_mm_uo'));

% Chattian no ice, glaciated (EAIS) and Rupelian ice (FAIS)
sst_chan  = rot90(ncread('../EO_data/tdluuo.pfclann.nc', 'temp_mm_uo'));
sst_chai  = rot90(ncread('../EO_data/tdluqo.pfclann.nc', 'temp_mm_uo'));
sst_chaii = rot90(ncread('../EO_data/tdwqfo.pfclann.nc', 'temp_mm_uo'));

% Priabonian no ice, EAIS and FAIS
sst_prin  = rot90(ncread('../EO_data/tdwqjo.pfclann.nc', 'temp_mm_uo'));
sst_prii  = rot90(ncread('../EO_data/tdwqmo.pfclann.nc', 'temp_mm_uo'));
sst_priii = rot90(ncread('../EO_data/tdwqlo.pfclann.nc', 'temp_mm_uo'));


%% Seasonal SST (only Priabonian and Rupelian)

% no ice summer
sst_rupns = rot90(ncread('../EO_data/tdluto.pfcldjf.nc', 'temp_mm_uo'));
sst_prins = rot90(ncread('../EO_data/tdwqjo.pfcldjf.nc', 'temp_mm_uo'));
% ice summer
sst_rupis = rot90(ncread('../EO_data/tdlupo.pfcldjf.nc', 'temp_mm_uo'));
sst_priis = rot90(ncread('../EO_data/tdwqlo.pfcldjf.nc', 'temp_mm_uo'));
% no ice winter
sst_rupnw = rot90(ncread('../EO_data/tdluto.pfcljja.nc', 'temp_mm_uo'));
sst_prinw = rot90(ncread('../EO_data/tdwqjo.pfcljja.nc', 'temp_mm_uo'));
% ice winter
sst_rupiw = rot90(ncread('../EO_data/tdlupo.pfcljja.nc', 'temp_mm_uo'));
sst_priiw = rot90(ncread('../EO_data/tdwqlo.pfcljja.nc', 'temp_mm_uo'));


%% Load SATs annual

% Rupelian no ice, glaciated (FAIS) and Chattian ice (EAIS)
t_rupn  = flipud(rot90(ncread('../EO_data/tdluta.pdclann.nc', 'temp_mm_1_5m')));
t_rupii = flipud(rot90(ncread('../EO_data/tdlupa.pdclann.nc', 'temp_mm_1_5m')));
t_rupi  = flipud(rot90(ncread('../EO_data/tdwqea.pdclann.nc', 'temp_mm_1_5m')));

% Chattian no ice, glaciated (EAIS) and Rupelian ice (FAIS)
t_chan  = flipud(rot90(ncread('../EO_data/tdluua.pdclann.nc', 'temp_mm_1_5m')));
t_chai  = flipud(rot90(ncread('../EO_data/tdluqa.pdclann.nc', 'temp_mm_1_5m')));
t_chaii = flipud(rot90(ncread('../EO_data/tdwqea.pdclann.nc', 'temp_mm_1_5m')));

% Priabonian no ice, EAIS and FAIS
t_prin  = flipud(rot90(ncread('../EO_data/tdwqja.pdclann.nc', 'temp_mm_1_5m')));
t_prii  = flipud(rot90(ncread('../EO_data/tdwqma.pdclann.nc', 'temp_mm_1_5m')));
t_priii = flipud(rot90(ncread('../EO_data/tdwqla.pdclann.nc', 'temp_mm_1_5m')));


%% Interpolate to lsm res

% generate grids to mesh between
[low_resX, low_resY] = meshgrid(lon, lat);
[hi_resX, hi_resY]   = meshgrid(lon_lsm, lat_lsm);

% interpolate SST
sst_int_rupn  = interp2(low_resX,low_resY,sst_rupn,hi_resX,hi_resY);
sst_int_rupii = interp2(low_resX,low_resY,sst_rupii,hi_resX,hi_resY);
sst_int_rupi  = interp2(low_resX,low_resY,sst_rupi,hi_resX,hi_resY);
sst_int_chan  = interp2(low_resX,low_resY,sst_chan,hi_resX,hi_resY);
sst_int_chai  = interp2(low_resX,low_resY,sst_chai,hi_resX,hi_resY);
sst_int_chaii = interp2(low_resX,low_resY,sst_chaii,hi_resX,hi_resY);
sst_int_prin  = interp2(low_resX,low_resY,sst_prin,hi_resX,hi_resY);
sst_int_prii  = interp2(low_resX,low_resY,sst_prii,hi_resX,hi_resY);
sst_int_priii = interp2(low_resX,low_resY,sst_priii,hi_resX,hi_resY);

% interpolate seasonal
sst_int_rupns = interp2(low_resX,low_resY,sst_rupns,hi_resX,hi_resY);
sst_int_rupis = interp2(low_resX,low_resY,sst_rupis,hi_resX,hi_resY);
sst_int_prins = interp2(low_resX,low_resY,sst_prins,hi_resX,hi_resY);
sst_int_priis = interp2(low_resX,low_resY,sst_priis,hi_resX,hi_resY);
sst_int_rupnw = interp2(low_resX,low_resY,sst_rupnw,hi_resX,hi_resY);
sst_int_rupiw = interp2(low_resX,low_resY,sst_rupiw,hi_resX,hi_resY);
sst_int_prinw = interp2(low_resX,low_resY,sst_prinw,hi_resX,hi_resY);
sst_int_priiw = interp2(low_resX,low_resY,sst_priiw,hi_resX,hi_resY);

% Interpolate SAT
t_int_rupn  = interp2(low_resX,low_resY,t_rupn,hi_resX,hi_resY);
t_int_rupii = interp2(low_resX,low_resY,t_rupii,hi_resX,hi_resY);
t_int_rupi  = interp2(low_resX,low_resY,t_rupi,hi_resX,hi_resY);
t_int_chan  = interp2(low_resX,low_resY,t_chan,hi_resX,hi_resY);
t_int_chai  = interp2(low_resX,low_resY,t_chai,hi_resX,hi_resY);
t_int_chaii = interp2(low_resX,low_resY,t_chaii,hi_resX,hi_resY);
t_int_prin  = interp2(low_resX,low_resY,t_prin,hi_resX,hi_resY);
t_int_prii  = interp2(low_resX,low_resY,t_prii,hi_resX,hi_resY);
t_int_priii = interp2(low_resX,low_resY,t_priii,hi_resX,hi_resY);

