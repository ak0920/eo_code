%% Plot topography and ice sheets (Figure 2 from Kennedy et al. 2015)
% Kennedy et al. 2015, Figure 2

%% Set some figure properties, load colour schemes etc.
latlim = [-90 90]; 
lonlim = [0 360];

load col_topo1.mat % aka colors2
load col_topo2.mat % aka colors3
load col_grey.mat

colors = colors2;
font = 'Times New Roman';
fontsz = 10;

which_color_bar = 'main'; % 'main' or 'sub'

%% Load boundary condition data
% land-sea mask, orography, bathymetry ice thickness and ice mask for all 3
% stages

oror    = rot90(ncread('../Getech/ancil/rup/rup_010.nc', 'oroguk'));
oroc    = rot90(ncread('../Getech/ancil/cha/cha_010.nc', 'oroguk'));
orop    = rot90(ncread('../Getech/ancil/pri/pri_010.nc', 'oroguk'));
batr    = rot90(ncread('../Getech/ancil/rup/rup_010.nc', 'bathuk'));
batc    = rot90(ncread('../Getech/ancil/cha/cha_010.nc', 'bathuk'));
batp    = rot90(ncread('../Getech/ancil/pri/pri_010.nc', 'bathuk'));
icer    = rot90(ncread('../Getech/ancil/rup/rup_025.nc', 'oroguk'));
icec    = rot90(ncread('../Getech/ancil/cha/cha_025.nc', 'oroguk'));
icep    = rot90(ncread('../Getech/ancil/pri/pri_025.nc', 'oroguk'));

% longitude and latidue (applies to all stages)
lon_lsm = double(ncread('../Getech/ancil/rup/rup_010.nc', 'longitude'));
lat_lsm = double(ncread('../Getech/ancil/rup/rup_010.nc', 'latitude'));

% load land-sea and ice masks
load_lsm

%% Make combined orography/bathymetry data set
% all 3 stages
orobr = oror;
orobr(batr>0) = -batr(batr>0);
orobc = oroc;
orobc(batc>0) = -batc(batc>0);
orobp = orop;
orobp(batp>0) = -batp(batp>0);

orobr = double(orobr); 
orobc = double(orobc); 
orobp = double(orobp); 


%% Find ice sheet
% this adds 7500m to the ice-capped orography so the colour bar can later
% be split into colour for 'standard' land elevation and greyscale for ice
% thickness/elevation

icehr  = icer-oror;
icehc  = icec-oroc;
icer(imr == 1) = double(icehr(imr == 1)+7500);
icec(imc == 1) = double(icehc(imc == 1)+7500);
icer(batr>0) = -batr(batr>0);
icec(batc>0) = -batc(batc>0);


maps = figure;

%% Global Plots
%% Priabonian
global1=subplot('position', [0.2 0.7 0.6 0.275]);

axesm('MapProjection','Robinson', 'MapLatLim', latlim,... 
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,flipud(orobp))
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])
cax = [-7500 7500];
caxis(cax)
colormap(colors);

% add coastline
hold on
plotm(lsm_pri_proc(2,:), lsm_pri_proc(1,:), 'k-'); 

% tidy up
text(-2.25,1.25, 'a','fontsize',fontsz,'fontname',font)
tightmap
box off
axis off

%% Rupelian
global2=subplot('position', [0.2 0.4 0.6 0.275]);

axesm('MapProjection','Robinson', 'MapLatLim', latlim,... 
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,flipud(orobr))
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])
cax = [-7500 7500];
caxis(cax)
colormap(colors);

% add coastline
hold on
plotm(lsm_rup_proc(2,:), lsm_rup_proc(1,:), 'k-'); 

% tidy up
text(-2.25,1.25, 'b','fontsize',fontsz,'fontname',font)
tightmap
box off
axis off
freezeColors


%% Ice cap Plots
%% Rupelian
sub1=axes('position', [0.2 0.1 0.25 0.25]);

axesm('MapProjection','stereo', 'MapLatLim', [-90 -45],...
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,flipud(icer))
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])
cax = [-7500 10500];
caxis(cax)
colormap(sub1,colors3);

% add coastline and ice mask
hold on
plotm(lsm_rup_proc(2,:)+1, lsm_rup_proc(1,:), 'k-');
plotm(im_rup_proc(2,:)+1, im_rup_proc(1,:), 'k-', 'linewidth', 2);

% tidy up
tightmap
box off
axis off
freezeColors

%% Chattian
sub2=axes('position', [0.525 0.1 0.25 0.25]);

axesm('MapProjection','stereo', 'MapLatLim', [-90 -45],...
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,flipud(icec))
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])
cax = [-7500 10500];
caxis(cax)

% add coastline and ice mask
hold on
plotm(lsm_cha_proc(2,:)+1, lsm_cha_proc(1,:), 'k-'); % LSM
plotm(im_cha_proc(2,:)+1, im_cha_proc(1,:), 'k-', 'linewidth', 2); % LSM

% tidy up
tightmap
box off
axis off
freezeColors


%% Add colorbar

if strcmp(which_color_bar,'main')

    subplot('position', [20 0 1 1]);
	imagesc(orobr);
	colormap(colors)
	cax = [-7500 7500];
	caxis(cax)
	colb1 = colorbar('location', 'WestOutside', 'Position', [0.115 0.475 0.022 0.4]);
	ylim(colb1,[-7000 6000])
	set(colb1, 'ytick', -7000:1000:6000)
	ylabel(colb1, 'Elevation (m)', 'fontname', font, 'fontsize', fontsz)
	set(gca, 'fontname', font, 'fontsize', fontsz)

else if strcmp(which_color_bar,'sub')
    % plot something out of the picture to set a new grey colour scheme 
	global4 = axes('position', [20 0 1 1]);
	cax2 = [0 3000];
	caxis(cax2)
	colormap(global4,col_grey);
	colorb2 = colorbar('location', 'WestOutside', 'Position', [0.115 0.125 0.022 0.2]);
	ylim(colorb2,[0 3000])
	set(colorb2, 'ytick', 0:1000:3000)
	ylabel(colorb2, 'Ice thickness (m)', 'fontname', font, 'fontsize', fontsz)
	set(gca, 'fontname', font, 'fontsize', fontsz)

    end
end

%% Final tidy up
truesize([550 510])
set(gcf, 'color', 'w');
