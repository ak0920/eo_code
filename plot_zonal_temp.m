% plot_zonal_temp
% Kennedy et al. 2015, Figure S6

load_sst_sat

% Some figiure properties
font = 'Times New Roman';
fontsz = 10;
linewidth = 2;


% Calculate zonal mean SSTs in the Southern Ocean
rnzm = nanmean(sst_rupn(61:73,:),1);
rizm = nanmean(sst_rupii(61:73,:),1);
cnzm = nanmean(sst_prin(61:73,:),1);
cizm = nanmean(sst_prii(61:73,:),1);

close(gcf)
figure


% Priabonian
subplot(2,1,1)
plot(1:96, cnzm, '-','color',[0.850000000000000,0.0872549042105675,0],'linewidth',linewidth)
hold on
plot(1:96, cizm, '--','color',[0.850000000000000,0.0872549042105675,0],'linewidth',linewidth)

set(gca,'xtick',[0,17,33,49,65,81,96],'fontname', font, 'fontsize', fontsz)
set(gca,'xticklabel',[0,60,120,180,-120,-60,0])
xlim([0 96])
ylim([0 14])
xlabel('Latitude (�E)')
ylabel('Mean SST >60�S (�C)')
title('Priabonian')
legend('Ice Free','Full AIS','location','SouthWest')


% Rupelian
subplot(2,1,2)
plot(1:96, rnzm, '-','color',[0.0585574759170413,0.364924505352974,0.679114878177643],'linewidth',linewidth)
hold on 
plot(1:96, rizm, '--','color',[0.0585574759170413,0.364924505352974,0.679114878177643],'linewidth',linewidth)
set(gca,'xtick',[0,17,33,49,65,81,96],'fontname', font, 'fontsize', fontsz)
set(gca,'xticklabel',[0,60,120,180,-120,-60,0])
xlim([0 96])
ylim([0 14])
title('Rupelian')
ylabel('Mean SST >60�S (�C)')
xlabel('Latitude (�E)')
legend('Ice Free','Full AIS','location','SouthWest')

set(gcf, 'color', 'w');

