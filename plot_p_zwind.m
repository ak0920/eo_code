%% Plot 500 hPa change and zonal winds
% Kennedy et al. 2015, Figure 4

% Load data
load_lsm
load_p
load_winds
load col_scale2.mat % aka scale_col2
load col_grey.mat

% Setting fig properties
latlim    = [-90 90]; 
lonlim    = [0 360];
col_range = [-150 150];

font   = 'Times New Roman';
fontsz = 10;

fig = figure;
set(fig, 'renderer', 'zbuffer');

%% 500 hPa change - Priabonian

subplot('position', [0.05 0.7 0.8 0.275]);

imagesc(flipud(P_int_priii(:,1:713)-P_int_prin(:,1:713)))
caxis(col_range)
colormap(scale_col2);

% add coastline
hold on
plot(lsm_pri_proc(1,:)*2+1, (lsm_pri_proc(2,:)-90)*-2+1,'k-')

% add gridlines
plot([1 720], [181 181], ':', 'color', [0.3 0.3 0.3])
plot([1 720], [301 301], ':', 'color', [0.3 0.3 0.3])
plot([121 121], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([241 241], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([361 361], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([481 481], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([601 601], [1 360], ':', 'color', [0.3 0.3 0.3])

% tidy up
text(-20, 140, 'a','fontsize',fontsz,'fontname',font)
ylim([120 360])
xlim([1 720])
set(gca, 'xticklabel', '', 'yticklabel','', 'xtick', [], 'ytick', [])


%% 500 hPa change - Rupelian

subplot('position', [0.05 0.4 0.8 0.275])

imagesc(flipud(P_int_rupii(:,1:713)-P_int_rupn(:,1:713)))
caxis(col_range)
colormap(scale_col2);

% add coastline
hold on
plot(lsm_rup_proc(1,:)*2+1, (lsm_rup_proc(2,:)-90)*-2+1,'k-')

% add gridlines
plot([1 720], [181 181], ':', 'color', [0.3 0.3 0.3])
plot([1 720], [301 301], ':', 'color', [0.3 0.3 0.3])
plot([121 121], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([241 241], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([361 361], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([481 481], [1 360], ':', 'color', [0.3 0.3 0.3])
plot([601 601], [1 360], ':', 'color', [0.3 0.3 0.3])

% tidy up
text(-20, 140, 'b','fontsize',fontsz,'fontname',font)
ylim([120 360])
xlim([1 720])
set(gca, 'xticklabel', '', 'yticklabel','', 'xtick', [], 'ytick', [])


%% Plot Zonal Winds

% Find mean zonal winds
uave_rupn = mean(u_rupn, 2);
uave_rupi = mean(u_rupi, 2);
uave_prii = mean(u_priii, 2);
uave_prin = mean(u_prin, 2);

% plot
subplot('position', [0.15 0.075 0.7 0.3])
plot(uave_prin, lat, '-','color', [0.0585574759170413,0.364924505352974,0.679114878177643], 'linewidth',2)
hold on
plot(uave_prii, lat, '--','color', [0.0585574759170413,0.364924505352974,0.679114878177643], 'linewidth',2)
plot(uave_rupn, lat, '-','color', [0.850000000000000,0.0872549042105675,0], 'linewidth',2)
plot(uave_rupi, lat, '--','color',[0.850000000000000,0.0872549042105675,0], 'linewidth',2)

% Plot zero line and gateway bounds
plot([0 0],[0 -90], ':', 'color' , [0.6 0.6 0.6])

plot([-4 6],[-58.75 -58.75], '--', 'color' , [0.6 0.6 0.6])
plot([-4 6],[-61.25 -61.25], '--', 'color' , [0.6 0.6 0.6])
text(-3.8,-55, 'Rupelian TS northern extent','fontsize',fontsz,'fontname',font)
text(-3.8,-64, 'Priabonian TS northern extent','fontsize',fontsz,'fontname',font)

% tidy up
text(-5,-35, 'c','fontsize',fontsz,'fontname',font)
ylim([-90 -30])
ylabel('Latitude (�N)', 'fontname', font, 'fontsize', fontsz)
xlabel('Mean zonal wind strength (m s^-^1)', 'fontname', font, 'fontsize', fontsz)
set(gca, 'fontname', font, 'fontsize', fontsz)
legend('Priabonian no AIS', 'Priabonian AIS', 'Rupelian no AIS', 'Rupelian AIS', 'location', 'SouthEast')

%% Add colorbar

subplot('position', [2 0 1 1])
caxis(col_range)
color = colorbar('location', 'EastOutside', 'Position', [0.87 0.5 0.022 0.4]);
set(color, 'ytick', min(col_range):30:max(col_range))
ylabel(color, 'Geopotential height (500 hPa) change (m)', 'fontname', font, 'fontsize', fontsz)
set(gca, 'fontname', font, 'fontsize', fontsz)


% final tidy up
truesize([580 510])
set(gcf, 'color', 'w');


