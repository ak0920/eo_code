% load_heatfluxes

hf_rupn  = csvread('../EO_data/tdlut_hf.csv');
hf_rupii = csvread('../EO_data/tdlup_hf.csv');
hf_rupi  = csvread('../EO_data/tdwqe_hf.csv');

hf_chan  = csvread('../EO_data/tdluu_hf.csv');
hf_chai  = csvread('../EO_data/tdluq_hf.csv');
% hf_chaii = csvread('../EO_data/tdwqf_hf.csv'); % this has not been downloaded

hf_prin  = csvread('../EO_data/tdwqj_hf.csv');
hf_prii  = csvread('../EO_data/tdwqm_hf.csv');
hf_priii = csvread('../EO_data/tdwql_hf.csv');
