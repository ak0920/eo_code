function [lsm_con_out] = proc_lsm(lsm)
% [lsm_con_out] = proc_lsm(lsm)
%
% This function processes a land-sea mask to produce x-y vectors for a
% coastline
% 
% Note: Sections at the end currently appear to be unused and will be removed
% later if everything still appears to work okay

[lsm_con] = contour(lsm,1); % Turn land-sea mask into coordinates using contour
lsm_con_x = lsm_con(1,:);    % Seperate into x and y
lsm_con_y = lsm_con(2,:);
close(gcf)                          % tidy up (close contour plot)


% turn coastline into degrees lat/lon for plotting
lsm_con_out(2,:) = (lsm_con_y-37)*2.5;   % centre the equator
lsm_con_out(1,:) = (lsm_con_x-0.5)*3.75; % small corresction for grid spacing

% Look through the coastline vectors to find gaps of greater than 2 degrees
% that imply a new land mass is being marked, and insert a gap (NaN):
for i=1:(length(lsm_con_out(1,:))-1);
    if lsm_con_out(1,i+1) - lsm_con_out(1,i)>8 || lsm_con_out(1,i+1) - lsm_con_out(1,i)<-8
        lsm_con_out(1,i) = NaN;lsm_con_out(2,i) = NaN;
    end
    if lsm_con_out(2,i+1) - lsm_con_out(2,i)>8 || lsm_con_out(2,i+1) - lsm_con_out(2,i)<-8
        lsm_con_out(1,i) = NaN;lsm_con_out(2,i) = NaN;
    end
end

        
        
        % %% Process High Res coast
% if length(lsm_rup(1,:)) ==720;
%     
%     % turn coastline into degrees lat/lon for plotting
%     lsm_con_rup1(2,:) = lsm_con_rupy(lsm_con_rup(2,:)<360 & lsm_con_rup(1,:)>1)/2-90; % centre the equator (and presumably remove some unrealistic values)
%     lsm_con_rup1(1,:) = lsm_con_rupx(lsm_con_rup(2,:)<360 & lsm_con_rup(1,:)>1)/2;    % correct grid spacing (and presumably remove some unrealistic values)
%     
%     % Look through the coastline vectors to find gaps of greater than 2 degrees
%     % that imply a new land mass is being marked, and insert a gap (NaN):
%     for i=1:(length(lsm_con_rup1(1,:))-1);
%         if lsm_con_rup1(1,i+1) - lsm_con_rup1(1,i)>2 || lsm_con_rup1(1,i+1) - lsm_con_rup1(1,i)<-2
%             lsm_con_rup1(1,i) = NaN;lsm_con_rup1(2,i) = NaN;
%         end
%         if lsm_con_rup1(2,i+1) - lsm_con_rup1(2,i)>2 || lsm_con_rup1(2,i+1) - lsm_con_rup1(2,i)<-2
%             lsm_con_rup1(1,i) = NaN;lsm_con_rup1(2,i) = NaN;
%         end
%     end
%     
%     
% %% Process Low Res coast
% else if length(lsm_rup(1,:)) ==96;

% %% Process Low Res coast
%     else if length(lsm_rup(1,:)) ==97;
%             
%             
%             lsm_con_rup1(2,:) = (lsm_con_rupy(lsm_con_rup(2,:)<360 & lsm_con_rup(1,:)>1)-37)*2.5;   % centre the equator (and presumably remove some unrealistic values)
%             lsm_con_rup1(1,:) = (lsm_con_rupx(lsm_con_rup(2,:)<360 & lsm_con_rup(1,:)>1)-0.5)*3.75; % correct grid spacing (and presumably remove some unrealistic values)
%             
%             % Look through the coastline vectors to find gaps of greater than 2 degrees
%             % that imply a new land mass is being marked, and insert a gap (NaN):
%             for i=1:(length(lsm_con_rup1(1,:))-1);
%                 if lsm_con_rup1(1,i+1) - lsm_con_rup1(1,i)>8 || lsm_con_rup1(1,i+1) - lsm_con_rup1(1,i)<-8
%                     lsm_con_rup1(1,i) = NaN;lsm_con_rup1(2,i) = NaN;
%                 end
%                 if lsm_con_rup1(2,i+1) - lsm_con_rup1(2,i)>8 || lsm_con_rup1(2,i+1) - lsm_con_rup1(2,i)<-8
%                     lsm_con_rup1(1,i) = NaN;lsm_con_rup1(2,i) = NaN;
%                 end
%             end
%             
%         end
%     end
% end

