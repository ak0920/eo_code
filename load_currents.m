% Load surface currents

%% u component

% Rupelian no ice, FAIS, EAIS
uc_rupn  = ncread('../EO_data/tdluno.pgclann.nc', 'ucurrTot_ym_dpth');
uc_rupii = ncread('../EO_data/tdlupo.pgclann.nc', 'ucurrTot_ym_dpth');
uc_rupi  = ncread('../EO_data/tdwqeo.pgclann.nc', 'ucurrTot_ym_dpth');

% Chattian no ice, EAIS, FAIS
uc_chan  = ncread('../EO_data/tdluoo.pgclann.nc', 'ucurrTot_ym_dpth');
uc_chai  = ncread('../EO_data/tdluqo.pgclann.nc', 'ucurrTot_ym_dpth');
uc_chaii = ncread('../EO_data/tdwqfo.pgclann.nc', 'ucurrTot_ym_dpth');

% Priabonian no ice, EAIS, FAIS
uc_prin  = ncread('../EO_data/tdwqjo.pgclann.nc', 'ucurrTot_ym_dpth');
uc_prii  = ncread('../EO_data/tdwqmo.pgclann.nc', 'ucurrTot_ym_dpth');
uc_priii = ncread('../EO_data/tdwqlo.pgclann.nc', 'ucurrTot_ym_dpth');


% select surface
uc_rupn  = flipud(rot90(uc_rupn(:,:,1)));
uc_rupii = flipud(rot90(uc_rupii(:,:,1)));
uc_rupi  = flipud(rot90(uc_rupi(:,:,1)));

uc_chan  = flipud(rot90(uc_chan(:,:,1)));
uc_chai  = flipud(rot90(uc_chai(:,:,1)));
uc_chaii = flipud(rot90(uc_chaii(:,:,1)));

uc_prin  = flipud(rot90(uc_prin(:,:,1)));
uc_prii  = flipud(rot90(uc_prii(:,:,1)));
uc_priii = flipud(rot90(uc_priii(:,:,1)));


% add row of zeros to match grid sizes
uc_rupn(73,:)  = 0;
uc_rupii(73,:) = 0;
uc_rupi(73,:)  = 0;

uc_chan(73,:)  = 0;
uc_chai(73,:)  = 0;
uc_chaii(73,:) = 0;

uc_prin(73,:)  = 0;
uc_prii(73,:)  = 0;
uc_priii(73,:) = 0;


%% v component

% Rupelian no ice, FAIS, EAIS
vc_rupn  = ncread('../EO_data/tdluno.pgclann.nc', 'vcurrTot_ym_dpth');
vc_rupii = ncread('../EO_data/tdwqeo.pgclann.nc', 'vcurrTot_ym_dpth');
vc_rupi  = ncread('../EO_data/tdlupo.pgclann.nc', 'vcurrTot_ym_dpth');

vc_chan  = ncread('../EO_data/tdluoo.pgclann.nc', 'vcurrTot_ym_dpth');
vc_chai  = ncread('../EO_data/tdluqo.pgclann.nc', 'vcurrTot_ym_dpth');
vc_chaii = ncread('../EO_data/tdwqfo.pgclann.nc', 'vcurrTot_ym_dpth');

vc_prin  = ncread('../EO_data/tdwqjo.pgclann.nc', 'vcurrTot_ym_dpth');
vc_prii  = ncread('../EO_data/tdwqmo.pgclann.nc', 'vcurrTot_ym_dpth');
vc_priii = ncread('../EO_data/tdwqlo.pgclann.nc', 'vcurrTot_ym_dpth');


% select syrface
vc_rupn  = flipud(rot90(vc_rupn(:,:,1)));
vc_rupii = flipud(rot90(vc_rupii(:,:,1)));
vc_rupi  = flipud(rot90(vc_rupi(:,:,1)));

vc_chan  = flipud(rot90(vc_chan(:,:,1)));
vc_chai  = flipud(rot90(vc_chai(:,:,1)));
vc_chaii = flipud(rot90(vc_chaii(:,:,1)));

vc_prin  = flipud(rot90(vc_prin(:,:,1)));
vc_prii  = flipud(rot90(vc_prii(:,:,1)));
vc_priii = flipud(rot90(vc_priii(:,:,1)));


% add row of zeros to match grid sizes
vc_rupn(73,:)  = 0;
vc_rupii(73,:) = 0;
vc_rupi(73,:)  = 0;

vc_chan(73,:)  = 0;
vc_chai(73,:)  = 0;
vc_chaii(73,:) = 0;

vc_prin(73,:)  = 0;
vc_prii(73,:)  = 0;
vc_priii(73,:) = 0;


%% Aggregate together/average winds
[curu_rupn, curv_rupn]   = proc_ave_winds(uc_rupn, vc_rupn);
[curu_rupi, curv_rupi]   = proc_ave_winds(uc_rupii, vc_rupi);
[curu_rupii, curv_rupii] = proc_ave_winds(uc_rupi, vc_rupii);

[curu_chan, curv_chan]   = proc_ave_winds(uc_chan, vc_chan);
[curu_chai, curv_chai]   = proc_ave_winds(uc_chai, vc_chai);
[curu_chaii, curv_chaii] = proc_ave_winds(uc_chaii, vc_chaii);

[curu_prin, curv_prin]   = proc_ave_winds(uc_prin, vc_prin);
[curu_prii, curv_prii]   = proc_ave_winds(uc_prii, vc_prii);
[curu_priii, curv_priii] = proc_ave_winds(uc_priii, vc_priii);

