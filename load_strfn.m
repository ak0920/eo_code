% load_strfn

% Load Stream Functions

% Rupelian no ice, glaciated (FAIS) and Chattian ice (EAIS)
str_rupn  = rot90(ncread('../EO_data/tdluto.pfclann.nc', 'streamFn_mm_uo'))/1e12;
str_rupii = rot90(ncread('../EO_data/tdlupo.pfclann.nc', 'streamFn_mm_uo'))/1e12;
str_rupi  = rot90(ncread('../EO_data/tdwqeo.pfclann.nc', 'streamFn_mm_uo'))/1e12;

% Chattian no ice, glaciated (EAIS) and Rupelian ice (FAIS)
str_chan  = rot90(ncread('../EO_data/tdluuo.pfclann.nc', 'streamFn_mm_uo'))/1e12;
str_chai  = rot90(ncread('../EO_data/tdluqo.pfclann.nc', 'streamFn_mm_uo'))/1e12;
str_chaii = rot90(ncread('../EO_data/tdwqfo.pfclann.nc', 'streamFn_mm_uo'))/1e12;

% Priabonian no ice, EAIS and FAIS
str_prin  = rot90(ncread('../EO_data/tdwqjo.pfclann.nc', 'streamFn_mm_uo'))/1e12;
str_prii  = rot90(ncread('../EO_data/tdwqmo.pfclann.nc', 'streamFn_mm_uo'))/1e12;
str_priii = rot90(ncread('../EO_data/tdwqlo.pfclann.nc', 'streamFn_mm_uo'))/1e12;

% Priabonian no ice and FAIS with corrected islands
str_prinI = rot90(ncread('../EO_data/tdwqko.pfclann.nc', 'streamFn_mm_uo'))/1e12;
str_priiI = rot90(ncread('../EO_data/tdwqvo.pfclann.nc', 'streamFn_mm_uo'))/1e12;
